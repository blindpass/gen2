// Copyright (C)2014 by Aaron Suen <warr1024@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package com.gmail.warr1024.blindpass2.cli;

import java.io.Console;

public class ConHelper
	{
	private ConHelper() { }

	private static Object conLock = new Object();
	private static Console con;
	private static Worker worker = null;

	public static void registerWorker(Worker w)
		{
		synchronized(conLock)
			{
			worker = w;
			}
		}

	public static Console getCon()
		{
		synchronized(conLock)
			{
			if(con == null)
				{
				con = System.console();
				if(con == null)
					throw new RuntimeException("Unable to access system console.");
				}
			return con;
			}
		}

	public static void workerReport()
		{
		synchronized(conLock)
			{
			if(worker != null)
				{
				Console con = getCon();
				StringBuilder sb = new StringBuilder();
				for(String s : worker.getStatus())
					{
					sb.append((sb.length() > 0) ? ": " : "");
					sb.append(s);
					}
				if(sb.length() > 0)
					con.format("& %s\n", sb.toString());
				}
			}
		}

	public static void writeLine(String line)
		{
		synchronized(conLock)
			{
			getCon().format("%s\n", line);
			}
		}

	public static void writeLine()
		{
		writeLine("");
		}

	public static void writePassword(String pref, char[] pass)
		{
		synchronized(conLock)
			{
			Console con = getCon();
			con.format("%s", pref);
			for(char c : pass)
				con.format("%c", c);
			con.format("\n");
			}
		}
	public static String readLine(String prompt)
		{
		synchronized(conLock)
			{
			workerReport();
			return getCon().readLine("%s: ", prompt);
			}
		}

	public static char[] readPassword(String prompt)
		{
		synchronized(conLock)
			{
			workerReport();
			return getCon().readPassword("%s: ", prompt);
			}
		}

	public static boolean confirm(String prompt)
		{
		synchronized(conLock)
			{
			workerReport();
			String answer = getCon().readLine("%s [y/n]: ", prompt);
			return answer.toUpperCase().startsWith("Y");
			}
		}
	}

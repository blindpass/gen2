// Copyright (C)2014 by Aaron Suen <warr1024@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package com.gmail.warr1024.blindpass2.cli;

import com.gmail.warr1024.blindpass2.BP2Database;
import com.gmail.warr1024.blindpass2.BP2GrowDB;
import com.gmail.warr1024.blindpass2.BP2Master;
import com.gmail.warr1024.blindpass2.BP2Random;
import com.gmail.warr1024.blindpass2.BP2Result;
import com.gmail.warr1024.blindpass2.BP2Search;
import com.gmail.warr1024.blindpass2.BP2SelfTest;
import com.gmail.warr1024.blindpass2.BP2Utility;
import java.lang.NumberFormatException;
import java.lang.Thread;
import java.nio.CharBuffer;

public class Main
	{
	private static char[] confirmPassword(String prompt)
		{
		char[] pass = null, conf = null;
		try
			{
			while(true)
				{
				pass = ConHelper.readPassword(prompt + " (blank to cancel)");
				if(pass.length < 1)
					return pass;
				conf = ConHelper.readPassword(prompt + " (repeat)");
				if(CharBuffer.wrap(pass).equals(CharBuffer.wrap(conf)))
					return pass;
				BP2Utility.destroyChars(pass, conf);
				ConHelper.writeLine("Passwords do not match, try again.");
				}
			}
		finally { BP2Utility.destroyChars(conf); }
		}

	private static void searchLoop(Worker worker, BP2Database filedb, final BP2Master master, String svc, BP2Search search)
		{
		while(true)
			{
			String cmd = ConHelper.readLine("Service: \"" + svc + "\""
				+ "\n  [S] Show search result(s)"
				+ "\n  [W] Write new password entry"
				+ "\n  [D] Delete search results"
				+ "\n  [Q] Cancel current search"
				+ "\nChoice").trim().toUpperCase();

			if(cmd.startsWith("S"))
				{
				boolean none = true;
				for(BP2Result result : search.getResults())
					if(result.getExists())
						{
						none = false;
						char[] pass = null;
						try
							{
							pass = result.getContent();
							ConHelper.writePassword("Password: ", pass);
							}
						finally { BP2Utility.destroyChars(pass); }
						}
				if(none)
					ConHelper.writeLine("No entries found yet.");
				}
			else if(cmd.startsWith("W") && ConHelper.confirm("All existing found entries for this service "
				+ "will be deleted!\nThis cannot be undone! Are you sure?"))
				{
				int selstop = 0;
				try { selstop = Integer.parseInt(ConHelper.readLine("Stop")); }
				catch(NumberFormatException ex) { }
				if(selstop < 1)
					{
					ConHelper.writeLine("Invalid stop number.");
					continue;
					}
				double estrtime = search.getEstimateSeconds(selstop, false);
				double estwtime = search.getEstimateSeconds(selstop, true);
				ConHelper.writeLine("Estimated write time: " + BP2Utility.secondsDisplay(estwtime));
				ConHelper.writeLine("Estimated read time: " + BP2Utility.secondsDisplay(estrtime));
				char[] newpass = null;
				try
					{
					newpass = confirmPassword("New password for \"" + svc + "\"");
					if(newpass.length > 0)
						{
						final int chosenstop = selstop;
						final BP2Search upsearch = search;
						final boolean[] finished = new boolean[1];
						final char[] uppass = newpass;
						search.addListener(new Runnable()
							{
							@Override
							public void run()
								{
								int mystop = 0;
								BP2Result picked = null;
								for(BP2Result result : upsearch.getResults())
									if(!result.getExists())
										{
										mystop++;
										if(mystop == chosenstop)
											{
											picked = result;
											break;
											}
										}
								if(picked == null)
									return;
								upsearch.stop();
								for(BP2Result result : upsearch.getResults())
									if(result.getExists())
										result.removeContent();
								picked.putContent(uppass);
								finished[0] = true;
								}
							}, true);
						long lastReport = System.currentTimeMillis();
						boolean firstLoop = true;
						while(!finished[0])
							{
							try { Thread.sleep(100); }
							catch(Exception ex) { throw new RuntimeException(ex); }
							if(firstLoop)	
								{
								ConHelper.writeLine("Save will complete when "
									+ "search reaches your chosen stop.");
								firstLoop = false;
								}
							long now = System.currentTimeMillis();
							if(now > lastReport + 2000)
								{
								lastReport = now;
								ConHelper.workerReport();
								}
							}
						break;
						}
					}
				finally { BP2Utility.destroyChars(newpass); }
				}
			else if(cmd.startsWith("D") && ConHelper.confirm("This cannot be undone! Are you sure?"))
				{
				search.stop();
				for(BP2Result result : search.getResults())
					if(result.getExists())
						result.removeContent();
				break;
				}
			else if(cmd.startsWith("Q") && ConHelper.confirm("Stop search and lose progress?"))
				break;
			}
		}

	private static void masterLoop(Worker worker, BP2Database filedb, final BP2Master master)
		{
		while(true)
			{
			final String svc = ConHelper.readLine("Service Name (blank to quit)");
			if(svc.length() < 1)
				{
				if(ConHelper.confirm("Quit?"))
					break;
				continue;
				}

			BP2Search search = null;
			try
				{
				search = new BP2Search(master, filedb, svc);
				final BP2Search upsearch = search;
				worker.add(search);
				
				searchLoop(worker, filedb, master, svc, search);
				}
			finally { search.destroy(); }
			}
		}

	public static void main(String[] args)
		{
		ConHelper.writeLine();
		ConHelper.writeLine(String.format("%s <%s>", BP2Utility.PRODUCT, BP2Utility.WEBSITE));
		ConHelper.writeLine(String.format("%s <%s>", BP2Utility.COPYRIGHT, BP2Utility.EMAIL));
		ConHelper.writeLine();
		for(String s : BP2Utility.LICENSE)
			ConHelper.writeLine(s + "\n");
		ConHelper.writeLine(String.format("JRE: %s %s, RNG: %s",
			System.getProperty("java.vendor"),
			System.getProperty("java.version"),
			new BP2Random().getAlgorithm()));
		ConHelper.writeLine();

		final Worker worker = new Worker();
		worker.start();
		ConHelper.registerWorker(worker);

		worker.add(new BP2SelfTest());

		final BP2GrowDB filedb = new BP2GrowDB(System.getProperty("user.home"), ".blindpass2db");
		worker.add(filedb);

		while(true)
			{
			final BP2Master master;
			char[] mpwd = null;
			try
				{
				mpwd = confirmPassword("Master Password");
				if(mpwd.length < 1)
					break;
				master = new BP2Master(mpwd);
				}
			finally { BP2Utility.destroyChars(mpwd); }

			try
				{
				worker.add(master);
				masterLoop(worker, filedb, master);
				}
			finally { master.destroy(); }
			}

		worker.stopWait();
		}
	}

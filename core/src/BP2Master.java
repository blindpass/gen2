// Copyright (C)2014 by Aaron Suen <warr1024@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package com.gmail.warr1024.blindpass2;

import java.nio.ByteBuffer;
import java.security.MessageDigest;

public class BP2Master implements BP2Task
	{
	public static final int HASH_COUNT = BP2Utility.MEM_SIZE / BP2Utility.HASH_LENGTH;
	public static final int ITER_MAX = HASH_COUNT * 2;

	private Object runLock = new Object();
	private byte[] masterPassBytes = null;
	private byte[] resultSecret = null;

	private volatile long iteration = 0;
	private volatile boolean stopRequested = false;

	public long getIteration() { return iteration; }
	public byte[] getSecret() { return resultSecret; }

	public String[] getStatus()
		{
		String[] status = new String[2];
		status[0] = "Signing in";
		status[1] = String.format("%.2f%%", (double)iteration / ITER_MAX * 100);
		return status;
		}

	public BP2Master(char[] newMasterPass)
		{
		try
			{
			masterPassBytes = BP2Utility.utf8Encode(newMasterPass);
			}
		catch(Exception ex)
			{
			throw new RuntimeException(ex);
			}
		}

	public void runCore()
		{
		if(resultSecret != null)
			return;
			
		iteration = 0;

		try
			{
			MessageDigest hash = BP2Utility.getHash();
			final int HASHLEN = BP2Utility.HASH_LENGTH;
			final int MEMSIZE = BP2Utility.MEM_SIZE;

			byte[] hashbuff = new byte[masterPassBytes.length + HASHLEN];
			System.arraycopy(masterPassBytes, 0, hashbuff, HASHLEN, masterPassBytes.length);

			byte[] membuff = new byte[HASHLEN * HASH_COUNT];
			int membuffidx = 0;
			for(iteration = 0; !stopRequested && (iteration < HASH_COUNT); iteration++)
				{
				hash.reset();
				hash.update(hashbuff);
				hash.digest(hashbuff, 0, HASHLEN);
				System.arraycopy(hashbuff, 0, membuff, membuffidx, HASHLEN);
				membuffidx += HASHLEN;
				}

			ByteBuffer hashwrap = ByteBuffer.wrap(hashbuff);
			for(iteration = HASH_COUNT; !stopRequested && (iteration < ITER_MAX); iteration++)
				{
				int readidx = (((hashwrap.getInt(0) % HASH_COUNT)
					+ HASH_COUNT) % HASH_COUNT) * HASHLEN;
				System.arraycopy(membuff, readidx, hashbuff, 0, HASHLEN);
				hash.reset();
				hash.update(hashbuff);
				hash.digest(hashbuff, 0, HASHLEN);
				}

			byte[] finalOut = new byte[HASHLEN];
			System.arraycopy(hashbuff, 0, finalOut, 0, HASHLEN);
			resultSecret = finalOut;

			for(int i = 0; i < masterPassBytes.length; i++)
				masterPassBytes[i] = 0;
			}
		catch(Exception ex)
			{
			throw new RuntimeException(ex);
			}
		}

	@Override
	public void run()
		{
		synchronized(runLock)
			{
			runCore();
			}
		}

	@Override
	public void stop()
		{
		stopRequested = true;
		}

	public void destroy()
		{
		stop();
		synchronized(runLock)
			{
			BP2Utility.destroyBytes(masterPassBytes);
			masterPassBytes = null;
			BP2Utility.destroyBytes(resultSecret);
			resultSecret = null;
			}
		}
	}

// Copyright (C)2014 by Aaron Suen <warr1024@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package com.gmail.warr1024.blindpass2;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.security.MessageDigest;

/**
 * A "utility" class, providing miscellaneous static methods and constants
 * used throughout BlindPass2.
 * <p>
 * Note that many of the values of constants and choices of algorithms
 * contained here are not "configurable" without breaking interoperability
 * with the reference implementation.
 */
public class BP2Utility
	{
	// "Static class" cannot be instantiated.
	private BP2Utility() { }

	// Cube root of 2, left-shifted to fill a long integer,
	// used by initBuffer.
	private static final long CUBE_ROOT_TWO = 5810360290122541960L;

	/**
	 * Standard size of memory buffer to use as a pool for "memory-hard"
	 * hashing algorithms.  This includes the "scrypt-like" fixed-iteration
	 * hash on the master password in {@link BP2Master}, and also the "rolling"
	 * hash in {@link BP2Search}.
	 */
	public static final int MEM_SIZE = 16 * 1024 * 1024;

	/**
	 * Expected size in bytes of the output of our chosen hash algorithm,
	 * in this case, SHA-256.  
	 */
	public static final int HASH_LENGTH = 256 / 8;

	/**
	 * Copyright statement for BlindPass2, hard-coded into the core layer
	 * to be consistent in the various UI layers.
	 */
	public static final String COPYRIGHT = "Copyright (C)2014 by Aaron Suen";

	/**
	 * An embedded copy of the full text of the license, for display in the UI.
	 */
	public static String[] LICENSE = {
		"Permission is hereby granted, free of charge, to any person obtaining a copy of\n"
		+ "this software and associated documentation files (the \"Software\"), to deal in\n"
		+ "the Software without restriction, including without limitation the rights to\n"
		+ "use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of\n"
		+ "the Software, and to permit persons to whom the Software is furnished to do so,\n"
		+ "subject to the following conditions:",
		"The above copyright notice and this permission notice shall be included in all\n"
		+ "copies or substantial portions of the Software.",
		"THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR\n"
		+ "IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS\n"
		+ "FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR\n"
		+ "COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER\n"
		+ "IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN\n"
		+ "CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE." };

	/**
	 * Author's email address, provided by core for UI consistency.
	 */
	public static final String EMAIL = "warr1024@gmail.com";

	/**
	 * BlindPass2 project "product name", provided by core for UI consistency.
	 */
	public static final String PRODUCT = "BlindPass2";

	/**
	 * BlindPass2 project website, provided by core for UI consistency.
	 */
	public static final String WEBSITE = "https://gitorious.org/blindpass/";

	/**
	 * Get an instance of the message digest algorithm used for most hashing
	 * operations in BlindPass2.  This must always be SHA-256.  Calling
	 * this method may be expensive, so its return value should be re-used
	 * if inside a performance-critical loop.
	 */
	public static MessageDigest getHash()
		{
		try
			{
			MessageDigest hash = MessageDigest.getInstance("SHA-256");
			if(hash.getDigestLength() != HASH_LENGTH)
				throw new RuntimeException("Expected hash length "
					+ Integer.toString(HASH_LENGTH) + ", got "
					+ Integer.toString(hash.getDigestLength()));
			return hash;
			}
		catch(Exception ex)
			{
			throw new RuntimeException(ex);
			}
		}

	/**
	 * Create a memory buffer, initialized with a deterministic pseudo-random
	 * bit sequence.
	 * <p>
	 * The specific bit sequence used is chosen because it will never repeat
	 * and always produces a Hamming weight of roughly 50% regardless of the
	 * requested size.
	 *
	 * @param	length	The number of bytes to allocate.
	 * @return		A newly allocated byte array, filled with
	 *			the pre-determined sequence of bits.
	 */
	public static byte[] initBuffer(int length)
		{
		byte[] buff = new byte[length];

		long spool = 0;
		int spoolbits = 0;
		long ctr = 0;
		int ctrbits = 1;

		int buffidx = 0;
		while(buffidx < buff.length)
			{
			// Add counter bits to the spool.
			spool = (spool << ctrbits) | (ctr & ((1 << ctrbits) - 1));
			spoolbits += ctrbits;

			// Increment the counter.
			ctr++;

			// If the counter exceeds 2**(n + 1/3), then increase the length
			// of the counter.  This "magic value" seems to balance the Hamming
			// weight out; larger or smaller may cause 0's or 1's to dominate.
			if(ctr >= (CUBE_ROOT_TWO >>> (63 - ctrbits)))
				ctrbits++;

			// Extract bits from the spool in byte-sized units.
			// Stop when the buffer is full.
			while((buffidx < buff.length) && (spoolbits >= 8))
				{
				buff[buffidx] = (byte)((spool >>> (spoolbits - 8)) & 255);
				spoolbits -= 8;
				buffidx++;
				}
			}

		return buff;
		}

	/**
	 * Destroy the contents of one or more byte arrays by overwriting
	 * the with zeros.  This is a rudimentary safety practice that should
	 * always be followed before allowing a buffer that contained
	 * security-sensitive information to be garbage-collected.
	 * <p>
	 * Note that this is not a strong guarantee that the data is actually
	 * removed from memory, as some virtual machines or operating systems
	 * may copy around memory for other reasons, such as defragmenting
	 * allocations.  Use of this method may help reduce, but does not
	 * necessarily eliminate, the risk of information leakage.
	 *
	 * @param	arr	Array(s) to clear.
	 */
	public static void destroyBytes(byte[]... arr)
		{
		if(arr != null)
			for(byte[] sub : arr)
				if(sub != null)
					for(int i = 0; i < sub.length; i++)
						sub[i] = 0;
		}

	/**
	 * Destroy the contents of one or more char arrays by overwriting
	 * the with zeros.  This is a rudimentary safety practice that should
	 * always be followed before allowing a buffer that contained
	 * security-sensitive information to be garbage-collected.
	 * <p>
	 * Note that this is not a strong guarantee that the data is actually
	 * removed from memory, as some virtual machines or operating systems
	 * may copy around memory for other reasons, such as defragmenting
	 * allocations.  Use of this method may help reduce, but does not
	 * necessarily eliminate, the risk of information leakage.
	 *
	 * @param	arr	Array(s) to clear.
	 */
	public static void destroyChars(char[]... arr)
		{
		if(arr != null)
			for(char[] sub : arr)
				if(sub != null)
					for(int i = 0; i < sub.length; i++)
						sub[i] = 0;
		}

	/**
	 * Safely convert a unicode char[] into its equivalent UTF-8 byte[].
	 *
	 * @param	chars	Character array to convert.
	 * @return		Equivalent bytes for chars encoded in UTF-8.
	 */
	public static byte[] utf8Encode(char[] chars)
		{
		return Charset.forName("UTF-8").encode(CharBuffer.wrap(chars)).array();
		}

	private static final String[] SIPREF = {"", "k", "M", "G", "T", "P", "E", "Z", "Y"};
	/**
	 * Display a number in human-readable format, using SI prefixes for brevity.
	 * This method tries to display a very large range of values using a limited
	 * number of characters.
	 * <p>
	 * Very small values (below 1) are not well-supported and may display fewer
	 * significant figures, or may appear to be zero.  Very large values
	 * (above 10**26) are not well-supported, and may display in longer scientific
	 * notation.
	 *
	 * @param	val	Numeric value to display.
	 * @return		A String representing the number in human-readable format.
	 */
	public static String rangeDisplay(double val)
		{
		int prefidx = 0;
		double sival = val;
		while((sival >= 1000) && (prefidx < SIPREF.length - 1))
			{
			prefidx++;
			sival /= 1000;
			}
		if(sival >= 1000)
			return String.format("%.0g", val);
		if(sival >= 100)
			return String.format("%.0f%s", sival, SIPREF[prefidx]);
		if(sival >= 10)
			return String.format("%.1f%s", sival, SIPREF[prefidx]);
		return String.format("%.2f%s", sival, SIPREF[prefidx]);
		}

	/**
	 * Display a span of time, given as a number of seconds, in human-readable
	 * form.  This format is somewhat "fuzzy" for larger values, which will be
	 * described in terms of larger units.  This is mainly useful for estimates,
	 * where precise values are not expected.
	 *
	 * @param	time	Duration of time interval, in seconds, to display.
	 * @return		A String representing the span of time in
	 *			human-readable format.
	 */
	public static String secondsDisplay(double time)
		{
		if(Double.isInfinite(time) || Double.isNaN(time))
			return "unknown";
		if(time <= 0)
			return "instant";
		if(time < 60)
			return BP2Utility.rangeDisplay(time) + " seconds";
		time /= 60;
		if(time < 60)
			return BP2Utility.rangeDisplay(time) + " minutes";
		time /= 60;
		if(time < 24)
			return BP2Utility.rangeDisplay(time) + " hours";
		time /= 24;
		if(time < 7)
			return BP2Utility.rangeDisplay(time) + " days";
		if(time < 30)
			return BP2Utility.rangeDisplay(time / 7) + " weeks";
		if(time < 365)
			return BP2Utility.rangeDisplay(time / 30) + " months";
		time /= 365;
			return BP2Utility.rangeDisplay(time) + " years";
		}

	// Helper method to fill a key/value pair with random chaff data,
	// usd by initChaff and putWithChaff.
	private static void fillChaff(BP2Random rand, ByteBuffer[] keys, byte[][] values, int idx)
		{
		// Create a new key with standard length.
		if(keys[idx] != null)
			destroyBytes(keys[idx].array());
		byte[] key = new byte[HASH_LENGTH];
		rand.getRngBytes(key);
		keys[idx] = ByteBuffer.wrap(key);

		// Create a new value with an exponentially-distributed random
		// length.  Note that size is constrained here by the BP2GrowDB
		// internal format; while other database formats may be used
		// in an actual application, we need to be sure to support GrowDB.
		destroyBytes(values[idx]);
		int chafflen = BP2Crypto.BLOCK_SIZE + (int)rand.getRngExpo(4);
		chafflen = (chafflen > Short.MAX_VALUE) ? Short.MAX_VALUE : chafflen;
		values[idx] = new byte[chafflen];
		rand.getRngBytes(values[idx]);
		}

	// Helper method to destroy key/value pairs used by initChaff and putWithChaff.
	private static void clearChaff(BP2Random rand, ByteBuffer[] keys, byte[][] values)
		{
		rand.destroy();
		if(keys != null)
			for(int i = 0; i < keys.length; i++)
				if(keys[i] != null)
					destroyBytes(keys[i].array());
		if(values != null)
			for(int i = 0; i < values.length; i++)
				destroyBytes(values[i]);
		}

	/**
	 * Initialize a database with inital chaff entries, if the database is
	 * empty.  This should be called immediately after initializing the database
	 * at application startup (before the user logs in) so that a non-empty
	 * database is not evidence that at least one real entry is stored there.
	 * <p>
	 * This method is non-destructive and can safely be called on any database,
	 * whether or not it has already been initialized.
	 *
	 * @param	db	Database to initialize.
	 */
	public static void initChaff(BP2Database db)
		{
		if(!db.keySet().isEmpty())
			return;

		// Generate and store an exponentially-distributed random number
		// of fake entries filled with random data.
		ByteBuffer[] keys = null;
		byte[][] values = null;
		BP2Random rand = new BP2Random();
		try
			{
			int qty = (int)rand.getRngExpo(4);
			keys = new ByteBuffer[qty];
			values = new byte[qty][];
			for(int i = 0; i < qty; i++)
				fillChaff(rand, keys, values, i);
			for(int i = 0; i < qty; i++)
				db.put(keys[i], values[i]);
			}
		finally { clearChaff(rand, keys, values); }
		}

	/**
	 * Add a record to a database, potentially padded with chaff entries.
	 * When multiple entries are written, the "real" entry may be at the
	 * beginning, middle, or end of the sequence, to minimize the risk that
	 * timing metadata that the database may store could be used to
	 * differentiate real entries from chaff.
	 *
	 * @param	db	Database into which to write the entry.
	 * @param	key	Key of the entry to write.
	 * @param	value	Payload for the entry, in encoded/encrypted form.
	 */
	public static void putWithChaff(BP2Database db, ByteBuffer key, byte[] value)
		{
		ByteBuffer[] keys = null;
		byte[][] values = null;
		BP2Random rand = new BP2Random();
		try
			{
			// Decide how many chaff entries to create.  Average is 1
			// chaff per real entry, but if the database is empty,
			// increase the amount of chaff to emulate initChaff.
			int bits = db.keySet().isEmpty() ? 3 : 1;
			int prechaff = (int)rand.getRngExpo(bits);
			int total = prechaff + 1 + (int)rand.getRngExpo(bits);

			// Spool results in a list, with the real entry in the "middle."
			// Timing differences between generating chaff vs. writing an
			// already-encoded value may be measurable in database implementations
			// that provide sufficiently precise timestamps, so they should
			// not be written directly as they're calculated.
			keys = new ByteBuffer[total];
			values = new byte[total][];
			for(int i = 0; i < total; i++)
				{
				if(i == prechaff)
					{
					keys[i] = key;
					values[i] = value;
					}
				else
					fillChaff(rand, keys, values, i);
				}
			prechaff = 0;

			// Flush all spooled entries to the database.
			for(int i = 0; i < total; i++)
				db.put(keys[i], values[i]);
			}
		finally { clearChaff(rand, keys, values); }
		}
	}

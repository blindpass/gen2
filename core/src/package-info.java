// Copyright (C)2014 by Aaron Suen <warr1024@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

/**
 * This package provides core functionality for all BlindPass2 password safe
 * and compatible applications.
 * <p>
 * A high-level overview of how to use BlindPass2 to access stored passwords:
 * <ol>
 * <li>Run built-in self-tests.  These will ensure that your environment is
 * sane and able to run BlindPass2, e.g. has the necessary built-in cryptographic
 * transforms available.  Instantiate a BP2SelfTest and run() it.
 * <li>Instantiate a BP2Database and run() it.  The BP2GrowDB is a standard
 * implementation that should be available on all platforms.  Custom implementations
 * can also be used.  The BP2Task interface members must be implemented, though
 * they can be stubbed out if the database requires no intialization.  However, it
 * is recommended that databases be initialized early, as a fast pre-loaded
 * keySet is highly recommended for searching.
 * <li>Once the user has supplied a Master Password, a BP2Master should be created
 * and run().
 * <li>Once the user has also supplied the name of a service to access, a BP2Search
 * must be instantiated, providing a BP2Database and BP2Master from previous steps.
 * If the master has not been run() yet, then run()ning the BP2Search will run()
 * the master.
 * <li>The BP2Search must be run() in a background thread or other asynchronous
 * process.  It will never terminate unless manually cancelled by the user (via
 * stop()).
 * <li>Any Runnables registered as listeners for the search will run (in the search
 * thread, i.e. blocking the search) when a result is found (including both existing
 * password entries, and new stops).  If the listener needs to interact with the
 * UI in a different thread, it may need to use an appropriate mechanism to invoke
 * code on that thread.
 * <li>The search will produce an array of BP2Results, to which new results will be
 * added progressively as found.  These BP2Results are locations at which passwords
 * are or may be stored; use getContent, putContent, or removeContent to handle
 * en/de-coding/cryption and database access automatically.
 * <li>The search must be stopped manually when the user decides that there are no
 * more results of interest (or after the user choses an action).  There is no way
 * to programmatically know when there will be no more search results of interest,
 * unless the user has provided search limit parameters (e.g. max depth) beforehand.
 * </ol>
 * <p>
 * The BP2TaskRunner may be used to help deal with a multithreaded user interface.
 * Start one up in a background thread, and deliver BP2Tasks to it as the user
 * provides the data necessary to define them.  Most BP2 classes that contain
 * potentially long-running operations, such as BP2Search and BP2Master, implement
 * BP2Task, so can be offloaded into the background this way.  The BP2TaskRunner
 * allows the UI to poll for status information.  This allows for a "pipelined"
 * user interface, which, while not necessarily the most intuitive workflow, takes
 * the best advantage of available CPU time, which may be important on slower systems.
 * <p>
 * Some caveats:
 * <ol>
 * <li>Most BP2 objects have a destroy() method that should be called when the
 * object is done being used (preferrably in a finally { } block when possible).
 * This will clear the internal state of the object to ensure private data is
 * wiped from memory as early as possible, instead of waiting for the garbage
 * collector to deallocate the object's storage (which may never happen in practice
 * until the process terminates).
 * <li>BP2Utility provides a destroyBytes and destroyChars method to destroy
 * byte[] and char[] objects, respectively.  These should be used to clear out
 * private arrays that may contain sensitive data.  Be careful when working with
 * data returned from BP2 object methods to ensure that the data returned is a
 * private copy, not a reference to an internal buffer (javadocs for the individual
 * methods should specify).
 * <li>Avoid placing sensitive information such as passwords or encryption keys
 * in Strings, as these are immutable, may be interned, and there is no way to
 * ensure that they are destroyed when done working with them (this is a common
 * Java best practice).  Keep passwords or other sensitive data in char[] or byte[]
 * objects.
 * <li>The Java runtime environment may still leak sensitive information if it
 * copies memory (e.g. for GC and/or defragmentation), and some JIT implementations
 * may even optimize out buffer clearing.  The underlying operating system may
 * do similar, or even write sensitive memory out unencrypted to swap space on
 * permanent storage.  There is no way to prevent any of this from inside the Java
 * runtime environment; the best that can be done is to follow best practices to
 * minimize the probability of damage.
 * </ol>
 */
package com.gmail.warr1024.blindpass2;

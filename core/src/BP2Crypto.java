// Copyright (C)2014 by Aaron Suen <warr1024@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package com.gmail.warr1024.blindpass2;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * A "utilty" class providing the symmetric encryption
 * transforms used in BlindPass2.
 */
public class BP2Crypto
	{
	// "Static class" cannot be constructed.
	private BP2Crypto() { }

	/**
	 * The block size, in bytes, of the cipher with the largest block size
	 * that is used in this engine.  All encrypt/decrypt transforms will
	 * require this number of bytes, minimum, as input; shorter inputs
	 * must be padded externally.  This is set by AES at 16 bytes.
	 */
	public static final int BLOCK_SIZE = 16;

	/**
	 * The size, in bytes, of encryption keys to use for all encryption
	 * transforms.  This is limited to 16 (128 bits) to allow use of the
	 * weakened providers included in some Java distributions due to
	 * import/export restrictions.
	 */
	public static final int KEY_SIZE = 16;

	/**
	 * Encrypt a byte array with the selected key and block cipher (by name),
	 * using a form of Ciphertext Stealing instead of padding.  The data must
	 * be at least BLOCK_SIZE bytes long, and will be overwritten in place
	 * with the ciphertext.  The key provided must be a valid key size for
	 * the cipher chosen.
	 * <p>
	 * Note that the cipher mode may not be interoperable with other implementations.
	 * For input lengths that are exactly a multiple of the cipher's block size
	 * (which may actually be smaller than BLOCK_SIZE), the data is encrypted using
	 * plain CBC mode with no padding.  Otherwise, ciphertext stealing is applied
	 * after encrypting the zero-padded data in CBC mode, as per
	 * <a href="https://en.wikipedia.org/wiki/Ciphertext_stealing#CBC_implementation_notes">Wikipedia:
	 * Ciphertext stealing</a>.
	 * <p>
	 * Initialization vector used is always zero.
	 *
	 * @param	cipher	Name of the encryption block algorithm to use.
	 * @param	data	Data to encrypt, will be overwritten with ciphertext.
	 * @param	key	Key to use for encryption.
	 */
	public static void ctsEncrypt(String cipher, byte[] data, byte[] key)
		{
		byte[] paddedplain = null, paddedcipher = null;
		try
			{
			// Select the cipher and do sanity checks on input.
			Cipher enc = Cipher.getInstance(cipher + "/CBC/NoPadding");
			int blocksize = enc.getBlockSize();
			if(blocksize > BLOCK_SIZE)
				throw new RuntimeException("Expected maximum block size "
					+ BLOCK_SIZE + ", but got " + blocksize + ".");
			if(data.length < BLOCK_SIZE)
				throw new RuntimeException("data must be at least " + BLOCK_SIZE
					+ " bytes long.");

			// Create the key and initialization vector.
			final SecretKeySpec keyspec = new SecretKeySpec(key, cipher);
			final IvParameterSpec iv = new IvParameterSpec(new byte[blocksize]);

			// Detect data that can be encrypted using simple CBC.
			int partialblock = data.length % blocksize;
			if(partialblock == 0)
				{
				enc.init(Cipher.ENCRYPT_MODE, keyspec, iv);
				paddedplain = enc.doFinal(data);
				System.arraycopy(paddedplain, 0, data, 0, data.length);
				return;
				}
			int padlength = data.length + blocksize - partialblock;

			// [1] pad the last block with zeros (manually here) and [2] encrypt.
			paddedplain = new byte[padlength];
			System.arraycopy(data, 0, paddedplain, 0, data.length);
			enc.init(Cipher.ENCRYPT_MODE, keyspec, iv);
			paddedcipher = enc.doFinal(paddedplain);

			// [3] Swap the last 2 ciphertext blocks, and [4] truncate.
			System.arraycopy(paddedcipher, 0, data, 0, padlength - blocksize * 2);
			System.arraycopy(paddedcipher, padlength - blocksize, data, padlength - blocksize * 2, blocksize);
			System.arraycopy(paddedcipher, padlength - blocksize * 2, data, padlength - blocksize, partialblock);
			}
		catch(Exception ex)
			{
			throw new RuntimeException(ex);
			}
		finally
			{
			BP2Utility.destroyBytes(paddedplain, paddedcipher);
			}
		}

	/**
	 * Decrypt data that was encrypted with ctsEncrypt.  See ctsEncrypt for details
	 * about the algorithm used.  To decrypt data, the same cipher and key must
	 * be used as was used to encrypt, or likely junk data will be returned.
	 *
	 * @param	cipher	Name of the decryption block algorithm to use.
	 * @param	data	Data to decrypt, will be overwritten with plaintext.
	 * @param	key	Key to use for decryption.
	 */
	public static void ctsDecrypt(String cipher, byte[] data, byte[] key)
		{
		byte[] secondlastcipher = null, secondlastplain = null, stolencipher = null, stolenplain = null;
		try
			{
			// Select the cipher and do sanity checks on input.
			Cipher dec = Cipher.getInstance(cipher + "/CBC/NoPadding");
			int blocksize = dec.getBlockSize();
			if(blocksize > BLOCK_SIZE)
				throw new RuntimeException("Expected maximum block size "
					+ BLOCK_SIZE + ", but got " + blocksize + ".");
			if(data.length < BLOCK_SIZE)
				throw new RuntimeException("data must be at least " + BLOCK_SIZE
					+ " bytes long.");

			// Create the key and initialization vector.
			final SecretKeySpec keyspec = new SecretKeySpec(key, cipher);
			final IvParameterSpec iv = new IvParameterSpec(new byte[blocksize]);

			// Detect data that would have been encrypted using simple CBC.
			int partialblock = data.length % blocksize;
			if(partialblock == 0)
				{
				dec.init(Cipher.DECRYPT_MODE, keyspec, iv);
				stolenplain = dec.doFinal(data);
				System.arraycopy(stolenplain, 0, data, 0, data.length);
				return;
				}

			int lastoffset = data.length - partialblock;
			int secondlastoffset = lastoffset - blocksize;

			// [1] Decrypt the second-last block.
			secondlastcipher = new byte[blocksize];
			System.arraycopy(data, secondlastoffset, secondlastcipher, 0, blocksize);
			dec.init(Cipher.DECRYPT_MODE, keyspec, iv);
			secondlastplain = dec.doFinal(secondlastcipher);

			// [2] Pad ciphertext with stolen plaintext, and [3] swap the last 2 blocks.
			stolencipher = new byte[secondlastoffset + blocksize * 2];
			System.arraycopy(data, 0, stolencipher, 0, secondlastoffset);
			System.arraycopy(data, lastoffset, stolencipher, secondlastoffset, partialblock);
			System.arraycopy(secondlastplain, partialblock, stolencipher, secondlastoffset + partialblock,
				blocksize - partialblock);
			System.arraycopy(secondlastcipher, 0, stolencipher, lastoffset, blocksize);

			// [4] Decrypt and [5] truncate.
			dec.init(Cipher.DECRYPT_MODE, keyspec, iv);
			stolenplain = dec.doFinal(stolencipher);
			System.arraycopy(stolenplain, 0, data, 0, data.length);
			}
		catch(Exception ex)
			{
			throw new RuntimeException(ex);
			}
		finally
			{
			BP2Utility.destroyBytes(secondlastcipher, secondlastplain, stolencipher, stolenplain);
			}
		}

	/**
	 * Encrypt/decrypt data using ARCFour-drop cipher.  This is a simple stream cipher,
	 * so the encrypt and decrypt operations are the same: they XOR the data with a
	 * pseudo-random (deterministic based on key) sequence of bits.  Weaknesses in the
	 * ARCFour cipher cause the beginning of this bit stream to leak key material, so
	 * ARCFour-drop drops a configurable number of bytes of stream output before using
	 * the remaining bytes to encrypt the message.
	 *
	 * @param	drop	Number of initial bytes to drop.
	 * @param	data	Data to en/de-crypt, will be overwritten in place.
	 * @param	key	Encryption key to use.
	 */
	public static void arcFourDrop(int drop, byte[] data, byte[] key)
		{
		byte[] in = null, out = null;
		try
			{
			final String cipher = "RC4";
			Cipher enc = Cipher.getInstance(cipher);
			final SecretKeySpec keyspec = new SecretKeySpec(key, cipher);
			enc.init(Cipher.DECRYPT_MODE, keyspec);

			in = new byte[drop + data.length];
			System.arraycopy(data, 0, in, drop, data.length);
			out = enc.doFinal(in);
			System.arraycopy(out, drop, data, 0, data.length);
			}
		catch(Exception ex)
			{
			throw new RuntimeException(ex);
			}
		finally
			{
			BP2Utility.destroyBytes(in, out);
			}
		}

	// Reverse and rotate a byte sequence, helper method for composite
	// encryption.  The reversal helps CBC/CTS mode diffuse in both
	// directions, while the rotation mixes adjacent blocks.
	private static void reverseRotate(byte[] data, int shift)
		{
		byte[] rot = null;
		try
			{
			rot = new byte[data.length];
			for(int i = 0; i < data.length; i++)
				rot[(rot.length - i + 4) % rot.length] = data[i];
			System.arraycopy(rot, 0, data, 0, data.length);
			}
		finally { BP2Utility.destroyBytes(rot); }
		}

	private static int COMP_ROUNDS = 4;

	// "Key schedule" algorithm for composite cipher, splits the incoming
	// 256-bit key data into the necessary 128-bit keys.
	private static byte[][] compositeKeys(byte[] key)
		{
		// Validate minimum input key size.
		final int hashlen = BP2Utility.HASH_LENGTH;
		int req = KEY_SIZE * 2;
		if(req < hashlen)
			req = hashlen;
		if(key.length < req)
			throw new RuntimeException("Expected key length "
				+ req + " or more, but got "
				+ key.length + ".");

		// Create output and temp buffers.
		byte[][] keys = new byte[COMP_ROUNDS * 3][];
		byte[] buff = null;
		try
			{
			// Create the buffer to fill with key material, with
			// size a multiple of hash length.
			int len = COMP_ROUNDS * 3 * KEY_SIZE;
			int mod = len % hashlen;
			if(mod != 0)
				len += hashlen - mod;
			buff = new byte[len];
			ByteBuffer bb = ByteBuffer.wrap(buff);

			// The raw input hash is used verbatim at the beginning
			// of the buffer.  Hashing may throw away some bits, so
			// we want to preserve them here.
			System.arraycopy(key, 0, buff, 0, key.length);

			// Hash the buffer contents to create each hash, and
			// write it into the buffer, until we've filled it.  The
			// buffer position is included in the hash to avoid
			// trivial cycles.
			MessageDigest hash = BP2Utility.getHash();
			for(int i = hashlen; i < buff.length; i += hashlen)
				{
				bb.position(len - 4);
				bb.putInt(i);
				hash.reset();
				hash.update(buff);
				hash.digest(buff, i, hashlen);
				}

			// Extract the necessary keys from the buffer.
			for(int i = 0; i < keys.length; i++)
				{
				keys[i] = new byte[KEY_SIZE];
				System.arraycopy(buff, i * KEY_SIZE, keys[i], 0, KEY_SIZE);
				}
			}
		catch(Exception ex)
			{
			BP2Utility.destroyBytes(keys);
			throw new RuntimeException(ex);
			}
		finally { BP2Utility.destroyBytes(buff); }

		return keys;
		}

	/**
	 * Encrypt data using a "composite" cipher, made up of multiple mathematically
	 * unrelated ciphers.  Each individual cipher uses a 128 bit key, so the overall
	 * scheme should be equivalent to no less than a 128-bit key cipher.
	 * <p>
	 * Note that although 256 bits of key material are used, this cipher may not
	 * provide the equivalent security as it may be vulnerable to a meet-in-the-middle
	 * attack (<a href="https://en.wikipedia.org/wiki/Meet-in-the-middle_attack">Wikipedia:
	 * Meet-in-the-middle attack</a>).
	 * <p>
	 * Each cipher is limited to 128 bits of key material due to possible import/export
	 * restrictions that affect some versions of the Java runtime.  Since AES-128 is
	 * considered to have about equivalent security as AES-256 despite the difference
	 * in key size, the extra bits are instead used to provide security by diversity.
	 * <p>
	 * The underlying algorithms used are Rijndael (AES), Blowfish, and ARCFour,
	 * chosen because all are commonly-used well-studied algorithms, but are believed
	 * to be completely unrelated to one another, and use different block sizes and/or
	 * modes of operation.  Any attack that weakens one should be unlikely to affect
	 * the others.
	 *
	 * @param	data	Data to be encrypted, will be overwritten in place.
	 * @param	key	Key to use to encrypt data.
	 */
	public static void compositeEncrypt(byte[] data, byte[] key)
		{
		byte[][] keys = new byte[0][];
		try
			{
			keys = compositeKeys(key);

			for(int round = 0; round < COMP_ROUNDS; round++)
				{
				ctsEncrypt("Blowfish", data, keys[round * 3]);
				reverseRotate(data, 4);
				ctsEncrypt("AES", data, keys[round * 3 + 1]);
				reverseRotate(data, -3);
				arcFourDrop(8192, data, keys[round * 3 + 2]);
				}
			}
		finally { BP2Utility.destroyBytes(keys); }
		}

	/**
	 * Decrypt data that was encrypted by compositeEncrypt.  See
	 * compositeEncrypt for details about the type of cipher(s) used.
	 *
	 * @param	data	Data to be encrypted, will be overwritten in place.
	 * @param	key	Key to use to encrypt data.
	 */
	public static void compositeDecrypt(byte[] data, byte[] key)
		{
		byte[][] keys = new byte[0][];
		try
			{
			keys = compositeKeys(key);
			for(int round = COMP_ROUNDS - 1; round >= 0; round--)
				{
				arcFourDrop(8192, data, keys[round * 3 + 2]);
				reverseRotate(data, -3);
				ctsDecrypt("AES", data, keys[round * 3 + 1]);
				reverseRotate(data, 4);
				ctsDecrypt("Blowfish", data, keys[round * 3]);
				}
			}
		finally { BP2Utility.destroyBytes(keys); }
		}
	}

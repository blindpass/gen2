// Copyright (C)2014 by Aaron Suen <warr1024@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package com.gmail.warr1024.blindpass2;

import java.util.concurrent.LinkedBlockingQueue;

public class BP2TaskRunner implements BP2Task
	{
	private LinkedBlockingQueue<BP2Task> queue = new LinkedBlockingQueue<BP2Task>();
	private Object curJobLock = new Object();
	private BP2Task curJob = null;
	private volatile boolean stopRequested = false;

	public void add(BP2Task job)
		{
		queue.add(job);
		}

	@Override
	public String[] getStatus()
		{
		BP2Task t = null;
		synchronized(curJobLock)
			{
			t = curJob;
			}
		if(t == null)
			return new String[0];
		return t.getStatus();
		}

	@Override
	public void run()
		{
		while(!stopRequested)
			{
			BP2Task job;
			try { job = queue.take(); }
			catch(Exception ex) { throw new RuntimeException(ex); }
			synchronized(curJobLock)
				{
				curJob = job;
				}
			job.run();
			synchronized(curJobLock)
				{
				curJob = null;
				}
			}
		}

	@Override
	public void stop()
		{
		final BP2TaskRunner self = this;
		queue.add(new BP2Task()
			{
			@Override
			public void run()
				{
				self.stopRequested = true;
				}

			@Override
			public String[] getStatus() { return new String[0]; }

			@Override
			public void stop() { }
			});
		for(BP2Task t : queue)
			t.stop();
		}
	}

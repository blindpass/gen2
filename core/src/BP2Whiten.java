// Copyright (C)2014 by Aaron Suen <warr1024@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package com.gmail.warr1024.blindpass2;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

/**
 * A "utilty" class providing the bijective "whitening" transform used
 * in BlindPass2.  This transformation removes statistical patterns from
 * a password, making it difficult to distinguish between real and random
 * data in encoded form.
 */
public class BP2Whiten
	{
	// "Static class" cannot be constructed.
	private BP2Whiten() { }

	// Code for "end of data" marker.
	private static final int WCODE_EOD = 95;

	// Code for "out of gamut" character, for encoding characters
	// outside of printable ASCII.
	private static final int WCODE_GAMUT= 96;

	/**
	 * Encode text with a statistical "whitening" transformation, which makes
	 * encoded data look statistically flat, and indistinguishable from random
	 * noise.  This is a bijective transformation, so any actual random noise
	 * passed through the corresponding decoder will result in data that has the
	 * same statistical indicators as a valid password.  Use of this transformation
	 * inside of encryption will cause the data, once decrypted and decoded, to
	 * ALWAYS look like a valid password, so statistical heuristics are less
	 * useful for brute-force cracking of a stored password.
	 * <p>
	 * The encoder will generate between 4 and 6 bytes per character of input,
	 * plus an exponentially-distributed random number of chaff bytes (no upper
	 * bound).
	 * <p>
	 * The data used, from BP2StatData, was originally produced by analyzing a
	 * large corpus of real-world leaked passwords.  Most passwords used only
	 * printable US-ASCII characters.  This encoding will not provide much protection
	 * to passwords that contain non-US-ASCII (i.e. Unicode) characters.
	 * <p>
	 * Most computer-generated passwords have statistically flat character
	 * probability distribution, and thus may stand out statistically from other
	 * passwords decoded by this method (the original corpus contained both hand-chosen
	 * and computer-generated passwords).  Ideally, to computer-generate a password
	 * that is maximally safe when encoded by this method, the password should be
	 * generated using the same statistical data.  One of the easiest ways to do that
	 * is to feed uniform random data into the whitening decoder.
	 *
	 * @param	pass	The password to encode.
	 * @return		The encoded password.  This array is allocated by the
	 *			method, and the caller is responsible for wiping it
	 *			when finished.
	 */
	public static byte[] whitenEncode(char[] pass)
		{
		int[][] stats = BP2StatData.getStats();

		BP2Random rand = new BP2Random();
		byte[] buff = null;
		byte[] chaff = null;
		try
			{
			// Create a buffer large enough to fit the worst-case
			// text content, i.e. all 4 byte integers for the code,
			// plus a 2-byte out-of-gamut character, plus at the very
			// end, room for an EOD/GAMUT character, which may be transferred
			// here from chaff, if chaff is long enough to need one.
			buff = new byte[pass.length * 6 + 4];
			ByteBuffer bb = ByteBuffer.wrap(buff);

			// Start with an "end of data" marker as the before-first
			// character, and encode each character based on the one
			// before it.
			int prevcode = WCODE_EOD;
			for(char c : pass)
				{
				// Assume by default that we'll have an out-of-gamut
				// character to encode.
				int wcode = WCODE_GAMUT;

				// If the character is a printable ASCII character, then
				// it's in-gamut.  Encode it with its own specific code,
				// except for a very small chance of using out-of-gamut anyway,
				// so that an out-of-gamut code followed by an in-gamut character
				// is not an indication to the decoder that this content was
				// NOT encoded using this method.
				if((c >= 32) && (c <= 126)
					&& (rand.getRngBits(16) != 0))
					wcode = c - 32;

				// Get the statistics for this code, given the previous, and
				// select a random value within this code's range.
				int[] s = stats[prevcode];
				int min = (wcode > 0) ? (s[wcode - 1] + 1) : Integer.MIN_VALUE;
				int max = s[wcode];

				// Print the code.  If this is an out-of-gamut character
				// code, print the actual character after it.
				bb.putInt((int)rand.getRngLasVegas(min, max));
				if(wcode == WCODE_GAMUT)
					bb.putChar(c);

				prevcode = wcode;
				}

			// Choose an exponentially-distributed random number of bytes to append
			// to the data as chaff, so that data not terminating exactly on a code
			// boundary isn't indicative of not-encoded-here data.
			int chafflen = (int)rand.getRngExpo(3);

			// Whitened data will be AES/CTS-encrypted, so the final data
			// MUST be at least a full block in length.
			int minsize = BP2Crypto.BLOCK_SIZE;
			if(bb.position() < minsize)
				chafflen += minsize - bb.position();

			// If chaff will fit an EOD character, then one is required to disambiguate
			// true end of data vs. that which could be decoded from chaff.  If there would
			// NOT be enough chaff to encode an out-of-gamut character, then treat out-of-gamut
			// as an alternative EOD.  Were we not to include out-of-gamut as an alernative
			// EOD, then something ending in out-of-gamut + 0 or 1 char is obviously not valid,
			// thus the decoder would know it had data that wasn't encoded here.
			if(chafflen >= 4)
				{
				int[] s = stats[prevcode];
				if(chafflen < 6)
					bb.putInt((int)rand.getRngLasVegas(s[WCODE_EOD - 1] + 1, s[WCODE_GAMUT]));
				else
					bb.putInt((int)rand.getRngLasVegas(s[WCODE_EOD - 1] + 1, s[WCODE_EOD]));
				chafflen -= 4;
				}

			// Create the actual chaff data as pure random.
			chaff = new byte[chafflen];
			rand.getRngBytes(chaff);

			// Concatenate the encoded data and chaff, and return.
			byte[] finalbytes = new byte[bb.position() + chafflen];
			System.arraycopy(buff, 0, finalbytes, 0, bb.position());
			System.arraycopy(chaff, 0, finalbytes, bb.position(), chafflen);
			return finalbytes;
			}
		catch(Exception ex)
			{
			throw new RuntimeException(ex);
			}
		finally
			{
			BP2Utility.destroyBytes(buff, chaff);
			}
		}

	/**
	 * Decode a password that was encoded using whitenEncode.  See the whitenEncode method
	 * for more details about the encoding.
	 *
	 * @param	buff	Encoded data from whitenEncode.
	 * @return		Decoded password.  This array is allocated by the method, and
	 *			the caller is responsible for clearing it when finished.
	 */
	public static char[] whitenDecode(byte[] buff)
		{
		int[][] stats = BP2StatData.getStats();

		char[] chars = null;
		try
			{
			// Allocate the worst-case amount of space needed, plus one
			// to guard against an off-by-one error (TODO: carefully audit
			// code to see if this guard is needed).
			chars = new char[buff.length / 4 + 1];
			int charpos = 0;

			// Read the bytes from the input buffer.  If we run out of
			// characters mid-symbol, that's actually indicative of a
			// legitimate end of data (the data may terminate with chaff
			// instead of an EOD character).
			ByteBuffer bb = ByteBuffer.wrap(buff);
			int prevcode = WCODE_EOD;
			try
				{
				while(true)
					{
					// Read and decode a 4-byte integer containing
					// a character code.
					int val = bb.getInt();
					int wcode = WCODE_EOD;
					int[] s = stats[prevcode];
					for(int i = 0; i < s.length; i++)
						if(s[i] >= val)
							{
							wcode = i;
							break;
							}

					// If the code indicates end-of-data, the rest is
					// chaff data and can be ignored; we're done.
					if(wcode == WCODE_EOD)
						break;

					// If the code indicates an out-of-gamut character,
					// the next character (if present, otherwise end of data)
					// is the Unicode character literal.  Otherwise, decode
					// the in-gamut character from the code itself.
					if(wcode == WCODE_GAMUT)
						chars[charpos] = bb.getChar();
					else
						chars[charpos] = (char)(wcode + 32);

					charpos++;
					prevcode = wcode;
					}
				}
			catch(BufferUnderflowException ex)
				{
				// Ignore; running out of data is a legitimate way to
				// indicate the end of data.  We could check for sufficient
				// remaining characters before reading and break instead,
				// but this is not a performance-critical code area, so
				// letting it throw an exception is fine.
				}

			// Truncate the buffer to only those characters we actually
			// got from decoding, and return.
			char[] finalchars = new char[charpos];
			System.arraycopy(chars, 0, finalchars, 0, charpos);
			return finalchars;
			}
		finally
			{
			BP2Utility.destroyChars(chars);
			}
		}
	}

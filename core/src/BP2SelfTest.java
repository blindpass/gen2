// Copyright (C)2014 by Aaron Suen <warr1024@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package com.gmail.warr1024.blindpass2;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;

public class BP2SelfTest implements BP2Task
	{
	private String status = null;

	@Override
	public String[] getStatus()
		{
		String s = status;
		String[] st = new String[(s == null) ? 1 : 2];
		st[0] = "Performing self-test...";
		if(s == null)
			return st;
		st[1] = s;
		return st;
		}

	private void testWhitening(BP2Random rand, String rangeName, long min, long max)
		{
		char[] pre = null, post = null;
		byte[] enc = null;
		for(int i = 0; i < 100; i++)
			try
				{
				status = String.format("Whitening %s %d%%", rangeName, i);
				int len = (int)rand.getRngExpo(8);
				if(len > 65535)
					len = 65535;
				pre = new char[len];
				for(int j = 0; j < len; j++)
					pre[j] = (char)rand.getRngLasVegas(min, max);
				enc = BP2Whiten.whitenEncode(pre);
				post = BP2Whiten.whitenDecode(enc);
				if(!CharBuffer.wrap(pre).equals(CharBuffer.wrap(post)))
					throw new RuntimeException("Whitener failed self-test");
				}
			finally
				{
				BP2Utility.destroyBytes(enc);
				BP2Utility.destroyChars(pre, post);
				}
		}

	private void testCipherSimple(BP2Random rand, String cipher)
		{
		byte[] pre = null, post = null, key = new byte[BP2Utility.HASH_LENGTH / 2];
		for(int i = 0; i < 100; i++)
			try
				{
				status = String.format("%s Encryption %d%%", cipher, i);
				int len = (int)rand.getRngExpo(8);
				if(len > 65535)
					len = 65535;
				if(len < BP2Crypto.BLOCK_SIZE)
					len = BP2Crypto.BLOCK_SIZE;
				pre = new byte[len];
				post = new byte[len];
				rand.getRngBytes(pre);
				System.arraycopy(pre, 0, post, 0, len);
				rand.getRngBytes(key);
				BP2Crypto.ctsEncrypt(cipher, post, key);
				BP2Crypto.ctsDecrypt(cipher, post, key);
				if(!ByteBuffer.wrap(pre).equals(ByteBuffer.wrap(post)))
					throw new RuntimeException(cipher + " Encryption failed self-test len " + len);
				}
			finally
				{
				BP2Utility.destroyBytes(pre, post, key);
				}
		}

	private void testCipherComposite(BP2Random rand)
		{
		byte[] pre = null, post = null, key = new byte[BP2Utility.HASH_LENGTH];
		for(int i = 0; i < 100; i++)
			try
				{
				status = String.format("Composite Encryption %d%%", i);
				int len = (int)rand.getRngExpo(8);
				if(len > 65535)
					len = 65535;
				if(len < BP2Crypto.BLOCK_SIZE)
					len = BP2Crypto.BLOCK_SIZE;
				pre = new byte[len];
				post = new byte[len];
				rand.getRngBytes(pre);
				System.arraycopy(pre, 0, post, 0, len);
				rand.getRngBytes(key);
				BP2Crypto.compositeEncrypt(post, key);
				BP2Crypto.compositeDecrypt(post, key);
				if(!ByteBuffer.wrap(pre).equals(ByteBuffer.wrap(post)))
					throw new RuntimeException("Composite Encryption failed self-test");
				}
			finally
				{
				BP2Utility.destroyBytes(pre, post, key);
				}
		}

	@Override
	public void run()
		{
		BP2Random rand = new BP2Random();
		try
			{
			testWhitening(rand, "ASCII", 32, 126);
			testWhitening(rand, "Unicode", 0, Integer.MAX_VALUE);
			testCipherSimple(rand, "AES");
			testCipherSimple(rand, "Blowfish");
			testCipherComposite(rand);
			// Test hashing
			}
		finally { rand.destroy(); }
		}

	@Override
	public void stop() { }
	}

// Copyright (C)2014 by Aaron Suen <warr1024@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package com.gmail.warr1024.blindpass2;

import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class BP2GrowDB implements BP2Database
	{
	private class Entry
		{
		long offset;
		int length;
		}

	private Path filePath = null;
	private HashMap<ByteBuffer, Entry> offsetCache
		= new HashMap<ByteBuffer, Entry>();
	private FileChannel file = null;
	private FileLock lock = null;
	private long enddata = 0;
	private volatile boolean stopRequested = false;
	private BP2Random rand = new BP2Random();

	public BP2GrowDB(String first, String... more)
		{
		filePath = Paths.get(first, more);
		}

	private volatile int loadEntries = 0;
	private volatile String exstatus = null;

	@Override
	public void run()
		{
		loadEntries = 0;
		try
			{
			exstatus = "Opening file";

			HashSet<StandardOpenOption> opts = new HashSet<StandardOpenOption>();
			opts.add(StandardOpenOption.CREATE);
			opts.add(StandardOpenOption.READ);
			opts.add(StandardOpenOption.SYNC);
			opts.add(StandardOpenOption.WRITE);

			FileChannel newfile;
			if(FileSystems.getDefault().supportedFileAttributeViews().contains("posix"))
				{
				HashSet<PosixFilePermission> perms = new HashSet<PosixFilePermission>();
				perms.add(PosixFilePermission.OWNER_READ);
				perms.add(PosixFilePermission.OWNER_WRITE);

				newfile = FileChannel.open(filePath, opts,
					PosixFilePermissions.asFileAttribute(perms));
				}
			else
				newfile = FileChannel.open(filePath, opts);

			exstatus = "Acquiring file lock";
			FileLock newlock = newfile.lock();

			exstatus = null;
			long newenddata = 0;
			HashMap<ByteBuffer, Entry> newOffsetCache = new HashMap<ByteBuffer, Entry>();
			while(!stopRequested && ((newfile.position() + BP2Utility.HASH_LENGTH
				+ BP2Crypto.BLOCK_SIZE) <= newfile.size()))
				{
				int bufflen = BP2Utility.HASH_LENGTH + 2;
				ByteBuffer buff = ByteBuffer.allocate(bufflen);
				if(newfile.read(buff) != bufflen)
					break;
				long offs = newfile.position();

				buff.flip();
				byte[] key = new byte[BP2Utility.HASH_LENGTH];
				buff.get(key);
				int len = buff.getShort();
				if(len < 0)
					break;
				len += BP2Crypto.BLOCK_SIZE;
				if(len > (newfile.size() - offs))
					break;

				Entry e = new Entry();
				e.offset = offs;
				e.length = len;
				newOffsetCache.put(ByteBuffer.wrap(key), e);

				newenddata = offs + len;
				newfile.position(newenddata);

				loadEntries++;
				}

			synchronized(this)
				{
				file = newfile;
				lock = newlock;
				offsetCache = newOffsetCache;
				enddata = newenddata;
				}
			}
		catch(Exception ex) { throw new RuntimeException(ex); }

		BP2Utility.initChaff(this);
		}

	@Override
	public String[] getStatus()
		{
		String[] status = new String[2];
		status[0] = "Loading DB \"" + filePath + "\"";
		if(exstatus != null)
			status[1] = exstatus;
		else
			status[1] = String.format("%,d %s", loadEntries,
				(loadEntries == 1) ? "Entry" : "Entries");
		return status;
		}

	@Override
	public void stop()
		{
		stopRequested = true;
		}

	@Override
	public Set<ByteBuffer> keySet()
		{
		return offsetCache.keySet();
		}

	public int size()
		{
		return offsetCache.size();
		}

	@Override
	public byte[] get(ByteBuffer key)
		{
		synchronized(this)
			{
			if(file == null)
				throw new RuntimeException("Cannot use a closed database.");
			}
		try
			{
			Entry e = offsetCache.get(key);
			if(e == null)
				return null;

			if((file.size() - e.offset) < e.length)
				throw new RuntimeException("Invalid payload size.");

			ByteBuffer p = ByteBuffer.allocate(e.length);
			file.position(e.offset);
			if(file.read(p) != e.length)
				throw new RuntimeException("Incorrect number of bytes read.");

			return p.array();
			}
		catch(Exception ex) { throw new RuntimeException(ex); }
		}

	@Override
	public void remove(ByteBuffer key)
		{
		synchronized(this)
			{
			if(file == null)
				throw new RuntimeException("Cannot use a closed database.");
			}
		try
			{
			Entry e = offsetCache.get(key);
			if(e == null)
				return;
			offsetCache.remove(key);

			byte[] newkey = new byte[BP2Utility.HASH_LENGTH];
			rand.getRngBytes(newkey);
			file.position(e.offset - 2 - newkey.length);
			file.write(ByteBuffer.wrap(newkey));

			byte[] newval = new byte[e.length];
			rand.getRngBytes(newval);
			file.position(e.offset);
			file.write(ByteBuffer.wrap(newval));

			offsetCache.put(ByteBuffer.wrap(newkey), e);
			}
		catch(Exception ex) { throw new RuntimeException(ex); }
		}

	@Override
	public void put(ByteBuffer key, byte[] val)
		{
		synchronized(this)
			{
			if(file == null)
				throw new RuntimeException("Cannot use a closed database.");
			}
		remove(key);

		if(key.array() == null)
			throw new RuntimeException("Key must be an array-backed ByteBuffer.");
		if(key.array().length != BP2Utility.HASH_LENGTH)
			throw new RuntimeException("Expected key length "
				+ BP2Utility.HASH_LENGTH + " but got "
				+ key.array().length + ".");

		if((val.length < BP2Crypto.BLOCK_SIZE) || (val.length > Short.MAX_VALUE))
			throw new RuntimeException("Expected value size between "
				+ BP2Crypto.BLOCK_SIZE + " and "
				+ Short.MAX_VALUE + " but got " + val.length + ".");
		int len = val.length - BP2Crypto.BLOCK_SIZE;

		Entry e = new Entry();
		e.offset = enddata + BP2Utility.HASH_LENGTH + 2;
		e.length = val.length;

		try
			{
			int total = BP2Utility.HASH_LENGTH + 2 + val.length;
			int chaff = (int)rand.getRngExpo(5);

			byte[] cb = new byte[chaff];
			rand.getRngBytes(cb);

			ByteBuffer bb = ByteBuffer.allocate(total + chaff);
			bb.put(key.array());
			bb.putShort((short)len);
			bb.put(val);
			bb.put(cb);
			bb.flip();
			file.position(enddata);
			file.write(bb);

			enddata += total;
			BP2Utility.destroyBytes(cb);
			BP2Utility.destroyBytes(bb.array());
			}
		catch(Exception ex) { throw new RuntimeException(ex); }

		byte[] keycopy = new byte[BP2Utility.HASH_LENGTH];
		System.arraycopy(key.array(), 0, keycopy, 0, keycopy.length);
		offsetCache.put(ByteBuffer.wrap(keycopy), e);
		}

	public void close()
		{
		synchronized(this)
			{
			try
				{
				if(lock != null)
					{
					lock.release();
					lock = null;
					}
				if(file != null)
					{
					file.force(true);
					file.close();
					file = null;
					}
				offsetCache = new HashMap<ByteBuffer, Entry>();
				enddata = 0;
				rand.destroy();
				rand = new BP2Random();
				}
			catch(Exception ex) { throw new RuntimeException(ex); }
			}
		}
	}

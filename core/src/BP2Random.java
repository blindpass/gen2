// Copyright (C)2014 by Aaron Suen <warr1024@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package com.gmail.warr1024.blindpass2;

import java.security.SecureRandom;

public class BP2Random
	{
	public static final long RNG_BYTES_MAX = 1048576;

	private long rngUsedBytes = 0;
	private SecureRandom rng = null;

	public String getAlgorithm()
		{
		return new SecureRandom().getAlgorithm();
		}

	public void getRngBytes(byte[] tofill)
		{
		rngUsedBytes += tofill.length;
		if((rng == null) || (rngUsedBytes > RNG_BYTES_MAX))
			{
			rng = new SecureRandom();
			rngUsedBytes = tofill.length;
			}
		rng.nextBytes(tofill);
		}

	private long rngPool = 0;
	private int rngPoolBits = 0;
	private byte[][] rngPoolBuffs = { {}, {0}, {0,0}, {0,0,0}, {0,0,0,0},
		{0,0,0,0,0}, {0,0,0,0,0,0}, {0,0,0,0,0,0,0}, {0,0,0,0,0,0,0,0} };
	public long getRngBits(int bits)
		{
		if(bits > 64)
			throw new RuntimeException("Cannot get " + bits
				+ " bits from pool, 64 max.");
		int wantbits = (64 - rngPoolBits) & 120;
		int needbits = bits - rngPoolBits;
		if(needbits <= 0)
			wantbits = 0;
		if(needbits > wantbits)
			wantbits = needbits;
		int wantbytes = (wantbits + 7) >>> 3;
		if(wantbytes > 0)
			{
			byte[] buff = rngPoolBuffs[wantbytes];
			getRngBytes(buff);
			for(byte b : buff)
				rngPool = (rngPool << 8) | ((long)b & 255);
			rngPoolBits += wantbytes << 3;
			if(rngPoolBits > 64)
				rngPoolBits = 64;
			}
		if(bits > rngPoolBits)
			throw new RuntimeException("Error trying to satisfy requested random bits, "
				+ rngPoolBits + " bits in pool, " + bits + " bits requested.");
		long chosen = (rngPool >>> (rngPoolBits - bits))
			& ((1 << bits) - 1);
		rngPoolBits -= bits;
		return chosen;
		}

	public long getRngLasVegas(long min, long max)
		{
		long range = max - min;
		if(range < 0)
			throw new RuntimeException("invalid getRngLasVegas range "
				+ range + ", min " + min + ", max " + max + ".");
		int bits = 0;
		for(long r = range; r > 0; r >>= 1)
			bits++;
		while(true)
			{
			long rand = getRngBits(bits);
			if(rand <= range)
				return min + rand;
			}
		}

	public long getRngExpo(int bits)
		{
		long qty = 0;
		while(getRngBits(bits) != 0)
			qty++;
		return qty;
		}

	public void destroy()
		{
		rng = null;
		rngUsedBytes = 0;
		rngPool = 0;
		rngPoolBits = 0;
		for(byte[] buff : rngPoolBuffs)
			BP2Utility.destroyBytes(buff);
		}
	}

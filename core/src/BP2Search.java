// Copyright (C)2014 by Aaron Suen <warr1024@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package com.gmail.warr1024.blindpass2;

import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class BP2Search implements BP2Task
	{
	private static final int INIT_SEC_FACTOR = 14;

	private BP2Master master = null;
	private String serviceName = null;
	private byte[] serviceNameBytes = null;
	private BP2Database database = null;

	private long startTime = 0;
	private long startIteration = 0;
	private volatile long iteration = 0;
	private ArrayList<BP2Result> results = null;
	private HashSet<Runnable> listeners = null;
	private volatile int found = 0;
	private volatile int stops = 0;

	public long getStartTime() { return startTime; }
	public long getStartIteration() { return startIteration; }
	public long getIteration()
		{
		if(iteration == 0)
			return master.getIteration();
		return iteration;
		}
	public double getSeconds()
		{
		if(startTime == 0)
			return 0D;
		return (System.currentTimeMillis() - startTime) / 1000.0;
		}
	public double getSpeed()
		{
		double time = getSeconds();
		if(time <= 3.0)
			return 0;
		return (iteration - startIteration) / time;
		}
	public double getEstimateSeconds(int stop, boolean fromNow)
		{
		double speed = getSpeed();
		if(speed <= 0)
			return Double.NaN;
		int maxstop = 0;
		for(BP2Result result : getResults())
			if(!result.getExists())
				{
				maxstop++;
				if(maxstop == stop)
					return fromNow ? 0 : (result.getTime() - startTime) / 1000.0;
				}

		double est = fromNow ? 0 : (System.currentTimeMillis() - startTime) / 1000.0;
		double coststop = maxstop;
		int endstop = 64 - INIT_SEC_FACTOR;
		for(int i = (stop > endstop) ? endstop: stop; i > maxstop; i--)  
			est += Math.pow(2, ((i > 64) ? 64 : i)
				+ INIT_SEC_FACTOR - 1) / speed;
		if(stop > endstop)
			est += Math.pow(2, 64) / speed * (stop - 64);

		return est;
		}
	public BP2Result[] getResults()
		{
		synchronized(results)
			{
			return results.toArray(new BP2Result[0]);
			}
		}

	public void addListener(Runnable listener, boolean runLockedNow)
		{
		synchronized(listeners)
			{
			listeners.add(listener);
			if(runLockedNow)
				listener.run();
			}
		}
	public void removeListener(Runnable listener)
		{
		synchronized(listeners)
			{
			listeners.remove(listener);
			}
		}

	private Object runLock = new Object();
	private volatile boolean stopRequested = false;
	private int securityFactor = INIT_SEC_FACTOR;
	private byte[] hashID = null;
	private byte[] hashKey = null;
	private byte[] membuff = null;
	private int membuffidx = 0;

	public BP2Search(BP2Master newMaster, BP2Database newDatabase, String newServiceName)
		{
		try { serviceNameBytes = newServiceName.getBytes("UTF8"); }
		catch(Exception ex) { throw new RuntimeException(ex); }
		if(newServiceName.length() < 25)
			serviceName = newServiceName;
		else
			serviceName = newServiceName.substring(0, 22) + "...";

		master = newMaster;
		database = newDatabase;

		final int HASHLEN = BP2Utility.HASH_LENGTH;

		hashID = BP2Utility.initBuffer(HASHLEN);
		hashKey = BP2Utility.initBuffer(HASHLEN);

		// Add a fake "wraparound" padding to the end of the buffer, and
		// copy wrapped bytes into there as needed.
		membuff = BP2Utility.initBuffer(BP2Utility.MEM_SIZE + HASHLEN);
		membuffidx = 0;

		results = new ArrayList<BP2Result>();
		listeners = new HashSet<Runnable>();
		}

	private void addResult(boolean exists)
		{
		byte[] saveID = new byte[hashID.length];
		System.arraycopy(hashID, 0, saveID, 0, hashID.length);
		byte[] saveKey = new byte[hashKey.length];
		System.arraycopy(hashKey, 0, saveKey, 0, hashKey.length);
		BP2Result result = new BP2Result(database, iteration,
			System.currentTimeMillis(), exists, saveID, saveKey);
		synchronized(results)
			{
			results.add(result);
			}

		Runnable[] torun = null;
		synchronized(listeners)
			{
			torun = listeners.toArray(new Runnable[0]);
			}
		if(exists)
			found++;
		else
			stops++;

		for(Runnable r : torun)
			r.run();
		}

	private void runCore()
		{
		BP2Random rand = null;
		byte[] hashbuff = null;
		try
			{
			if(stopRequested)
				return;

			if(master.getSecret() == null)
				master.run();
			iteration = master.getIteration();

			rand = new BP2Random();

			final MessageDigest hash = BP2Utility.getHash();
			final int HASHLEN = BP2Utility.HASH_LENGTH;
			final int MEMSIZE = BP2Utility.MEM_SIZE;
			final int MEMWRAP = MEMSIZE - HASHLEN;

			byte[] masterbytes = master.getSecret();
			hashbuff = new byte[HASHLEN * 2 + masterbytes.length + serviceNameBytes.length];
			ByteBuffer hashwrap = ByteBuffer.wrap(hashbuff);
			System.arraycopy(masterbytes, 0, hashbuff, HASHLEN, masterbytes.length);
			System.arraycopy(serviceNameBytes, 0, hashbuff, HASHLEN + masterbytes.length, serviceNameBytes.length);

			Set<ByteBuffer> dbKeys = database.keySet();
			ByteBuffer idwrap = ByteBuffer.wrap(hashID);
			ByteBuffer keywrap = ByteBuffer.wrap(hashKey);

			startTime = System.currentTimeMillis();
			startIteration = iteration;

			byte[] temp = null;
			ByteBuffer tempwrap = null;
			while(!stopRequested)
				{
				iteration++;

				temp = hashID;
				tempwrap = idwrap;
				hashID = hashKey;
				idwrap = keywrap;
				hashKey = temp;
				keywrap = tempwrap;

				System.arraycopy(hashKey, 0, hashbuff, 0, HASHLEN);
				int readidx = ((hashwrap.getInt(0) % MEMSIZE) + MEMSIZE) % MEMSIZE;
				if(readidx > MEMWRAP)
					System.arraycopy(membuff, 0, membuff, MEMSIZE, HASHLEN);
				System.arraycopy(membuff, readidx, hashbuff, HASHLEN, HASHLEN);

				hash.reset();
				hash.update(hashbuff);
				hash.digest(hashID, 0, HASHLEN);
			
				System.arraycopy(hashID, 0, membuff, membuffidx, HASHLEN);
				membuffidx = (membuffidx + HASHLEN) % MEMSIZE;

				if(dbKeys.contains(idwrap))
					addResult(true);
				else if(rand.getRngBits(securityFactor) == 0L)
					{
					addResult(false);
					if(securityFactor < 64)
						securityFactor++;
					}
				}

			startTime = 0;
			startIteration = 0;
			}
		catch(Exception ex)
			{
			throw new RuntimeException(ex);
			}
		finally
			{
			if(rand != null)
				rand.destroy();
			BP2Utility.destroyBytes(hashbuff);
			}
		}

	@Override
	public void run()
		{
		synchronized(runLock)
			{
			runCore();
			}
		}

	@Override
	public String[] getStatus()
		{
		String[] status = new String[2];
		status[0] = "Searching for \"" + serviceName + "\"";
		double speed = getSpeed();
		if(speed <= 0)
			status[1] = String.format("Found %,d Stops %,d Depth %s",
				found, stops, BP2Utility.rangeDisplay(iteration));
		else
			status[1] = String.format("Found %,d Stops %,d Depth %s Speed %s",
				found, stops, BP2Utility.rangeDisplay(iteration),
				BP2Utility.rangeDisplay(speed));
		return status;
		}

	@Override
	public void stop()
		{
		stopRequested = true;
		}

	public void destroy()
		{
		stop();
		synchronized(runLock)
			{
			BP2Utility.destroyBytes(hashID);
			hashID = null;
			BP2Utility.destroyBytes(hashKey);
			hashKey = null;
			BP2Utility.destroyBytes(membuff);
			membuff = null;
			synchronized(results)
				{
				for(BP2Result r : results)
					r.destroy();
				}
			}
		}
	}

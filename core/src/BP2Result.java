// Copyright (C)2014 by Aaron Suen <warr1024@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package com.gmail.warr1024.blindpass2;

import java.nio.ByteBuffer;

public class BP2Result
	{
	private BP2Database database;
	private long iteration;
	private long time;
	private boolean exists;
	private byte[] id;
	private byte[] key;
	private ByteBuffer idbuff;

	public BP2Database getDatabase() { return database; }
	public long getIteration() { return iteration; }
	public long getTime() { return time; }
	public boolean getExists() { return exists; }
	public byte[] getID() { return id; }
	public byte[] getKey() { return key; }

	public BP2Result(BP2Database newDatabase, long newIteration, long newTime,
		boolean newExists, byte[] newID, byte[] newKey)
		{
		database = newDatabase;
		iteration = newIteration;
		time = newTime;
		exists = newExists;
		id = newID;
		key = newKey;
		idbuff = ByteBuffer.wrap(id);
		}

	public char[] getContent()
		{
		if(!exists || !database.keySet().contains(idbuff))
			return null;
		byte[] data = null;
		try
			{
			data = database.get(idbuff);
			if(data == null)
				return null;
			BP2Crypto.compositeDecrypt(data, key);
			return BP2Whiten.whitenDecode(data);
			}
		finally { BP2Utility.destroyBytes(data); }
		}

	public void putContent(char[] toSave)
		{
		byte[] data = null;
		try
			{
			data = BP2Whiten.whitenEncode(toSave);
			BP2Crypto.compositeEncrypt(data, key);
			BP2Utility.putWithChaff(database, idbuff, data);
			}
		finally { BP2Utility.destroyBytes(data); }
		}

	public void removeContent()
		{
		database.remove(idbuff);
		}

	public void destroy()
		{
		BP2Utility.destroyBytes(id);
		id = null;
		BP2Utility.destroyBytes(key);
		key = null;
		}
	}

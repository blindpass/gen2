// Copyright (C)2014 by Aaron Suen <warr1024@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package com.gmail.warr1024.blindpass2.gui;

import com.gmail.warr1024.blindpass2.BP2Result;
import com.gmail.warr1024.blindpass2.BP2Search;
import com.gmail.warr1024.blindpass2.BP2Utility;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.SwingUtilities;

public class UIService implements Runnable
	{
	private Main main = null;

	public UIService(Main newMain)
		{
		main = newMain;
		}

	private void updateList()
		{
		final BP2Search search = main.getSearch();
		final Layout layout = main.getLayout();
		if(layout == null)
			return;

		DefaultListModel<String> dlm = (DefaultListModel<String>)layout.lstRead.getModel();
		dlm.clear();
		if(layout.lstRead.isShowing() && (search != null))
			{
			for(BP2Result r : search.getResults())
				if(r.getExists())
					{
					char[] pwd = r.getContent();
					dlm.addElement(new String(pwd));
					BP2Utility.destroyChars(pwd);
					}
			}
		}

	@Override
	public void run()
		{
		final Layout layout = main.getLayout();

		layout.btnServiceCancel.addActionListener(new ActionListener()
			{
			@Override
			public void actionPerformed(ActionEvent e)
				{
				main.signOut();
				}
			});

		layout.btnServiceOK.addActionListener(new ActionListener()
			{
			@Override
			public void actionPerformed(ActionEvent e)
				{
				String svc = layout.txtService.getText();
				if(svc.length() < 1)
					{
					main.alertError("Please enter a service name.");
					return;
					}
				layout.txtService.setText("");
				BP2Search search = new BP2Search(main.getMaster(),
					main.getDatabase(), svc);
				search.addListener(new Runnable()
					{
					@Override
					public void run()
						{
						SwingUtilities.invokeLater(new Runnable()
							{
							@Override
							public void run()
								{
								updateList();	
								}
							});
						}
					}, true);
				main.setSearch(search);
				layout.cardOpts();
				}
			});

		layout.btnOptEnd.addActionListener(new ActionListener()
			{
			@Override
			public void actionPerformed(ActionEvent e)
				{
				main.cancelSearch();
				}
			});

		layout.btnOptShow.addActionListener(new ActionListener()
			{
			@Override
			public void actionPerformed(ActionEvent e)
				{
				layout.cardRead();
				updateList();
				}
			});

		layout.btnReadEnd.addActionListener(new ActionListener()
			{
			@Override
			public void actionPerformed(ActionEvent e)
				{
				layout.cardOpts();
				updateList();
				}
			});

		layout.btnOptDelete.addActionListener(new ActionListener()
			{
			@Override
			public void actionPerformed(ActionEvent e)
				{
				BP2Search cur = main.getSearch();
				if(cur == null)
					{
					main.alertError("No search running.");
					return;
					}
				BP2Result[] results = cur.getResults();
				int found = 0;
				for(BP2Result r : results)
					if(r.getExists())
						found++;
				if(found < 1)
					{
					main.alertInfo("No results found yet.");
					return;
					}
				if(!main.confirm("Delete " + found + " result(s)?", "Delete"))
					return;
				for(BP2Result r : results)
					if(r.getExists())
						r.removeContent();
				main.setSearch(null);
				layout.cardService();
				main.alertInfo("Password(s) deleted.");
				}
			});

		layout.btnOptWrite.addActionListener(new ActionListener()
			{
			@Override
			public void actionPerformed(ActionEvent e)
				{
				layout.cardWrite();
				}
			});
		}
	}

// Copyright (C)2014 by Aaron Suen <warr1024@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package com.gmail.warr1024.blindpass2.gui;

import com.gmail.warr1024.blindpass2.BP2Database;
import com.gmail.warr1024.blindpass2.BP2GrowDB;
import com.gmail.warr1024.blindpass2.BP2Master;
import com.gmail.warr1024.blindpass2.BP2Random;
import com.gmail.warr1024.blindpass2.BP2Search;
import com.gmail.warr1024.blindpass2.BP2SelfTest;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.Font;
import java.awt.SplashScreen;
import java.lang.StackTraceElement;
import java.lang.StringBuilder;
import java.lang.Thread;
import java.util.HashSet;
import java.util.Set;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.WindowConstants;

public class Main implements Runnable, WindowListener
	{
	private Worker worker = null;
	private Layout layout = null;
	private Timer tickTimer = null;
	private Runnable[] uiParts = null;
	private HashSet<Runnable> onTick = new HashSet<Runnable>();
	private BP2GrowDB database = null;
	private BP2Master master = null;
	private BP2Search search = null;

	public Set<Runnable> getOnTick() { return onTick; }

	public Worker getWorker() { return worker; }

	public Layout getLayout() { return layout; }

	public BP2Database getDatabase() { return database; }

	public BP2Master getMaster() { return master; }

	public void setMaster(BP2Master newmaster)
		{
		if(newmaster == master)
			return;
		setSearch(null);
		if(master != null)
			{
			master.stop();
			master.destroy();
			}
		master = newmaster;
		if(master != null)
			worker.add(master);
		}

	public BP2Search getSearch() { return search; }

	public void setSearch(BP2Search newsearch)
		{
		if(newsearch == search)
			return;
		if(search != null)
			{
			search.stop();
			search.destroy();
			}
		search = newsearch;
		if(search != null)
			worker.add(search);
		}

	public void alertError(Object msg)
		{
		JOptionPane.showMessageDialog(layout.frame.getContentPane(),
			msg, "Error", JOptionPane.ERROR_MESSAGE);
		}

	public void alertInfo(Object msg)
		{
		JOptionPane.showMessageDialog(layout.frame.getContentPane(),
			msg, "Message", JOptionPane.INFORMATION_MESSAGE);
		}

	public boolean confirm(Object question, String title)
		{
		return JOptionPane.showConfirmDialog(layout.frame.getContentPane(),
			question, title, JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)
			== JOptionPane.YES_OPTION;
		}

	public boolean cancelSearch()
		{
		if(search == null)
			return true;
		if(!confirm("Terminate current search?  Progress will be lost.", "Terminate Search"))
			return false;
		setSearch(null);
		if(master == null)
			layout.cardMaster();
		else
			layout.cardService();
		return true;
		}

	public boolean signOut()
		{
		if(!cancelSearch())
			return false;
		if(master == null)
			return true;
		if(!confirm("Are you sure you want to sign out?", "Sign Out"))
			return false;
		setMaster(null);
		layout.cardMaster();
		return true;
		}

	public void shutdown()
		{
		setSearch(null);
		setMaster(null);
		if(worker != null)
			worker.stopWait();
		if(database != null)
			database.close();
		if(tickTimer != null)
			tickTimer.stop();
		if(layout != null)
			layout.frame.dispose();
		}

	public void userQuit()
		{
		if(!signOut())
			return;
		shutdown();
		}

	public void updateStatus()
		{
		Layout l = layout;
		if(l == null)
			return;

		String[] status = new String[0];
		Worker w = worker;
		if(w != null)
			status = w.getStatus();
		if(status.length > 0)
			{
			l.barProgress.setIndeterminate(true);
			StringBuilder sb = new StringBuilder();
			for(String s : status)
				{
				if(sb.length() > 0)
					sb.append("<br>");
				sb.append(s);
					}
			l.lblProgress.setText("<html>" + sb.toString());
			}
		else
			{
			l.barProgress.setIndeterminate(false);
			l.lblProgress.setText("Ready...");
			}
		}

	@Override
	public void run()
		{
		final Main self = this;

		layout = new Layout();

		layout.lblJreInfo.setText(String.format("JRE: %s %s, RNG: %s",
			System.getProperty("java.vendor"),
			System.getProperty("java.version"),
			new BP2Random().getAlgorithm()));

		layout.frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
		layout.frame.addWindowListener(this);

		uiParts = new Runnable[3];
		uiParts[0] = new UIMaster(this);
		uiParts[1] = new UIService(this);
		uiParts[2] = new UIWrite(this);

		layout.btnCopyOK.addActionListener(new ActionListener()
			{
			@Override
			public void actionPerformed(ActionEvent e)
				{
				layout.cardMaster();
				}
			});
		layout.btnCopyQuit.addActionListener(new ActionListener()
			{
			@Override
			public void actionPerformed(ActionEvent e)
				{
				userQuit();
				}
			});
			
		layout.cardCopy();
		layout.frame.setVisible(true);

		for(Runnable r : uiParts)
			r.run();

		tickTimer = new Timer(250, new ActionListener()
			{
			@Override
			public void actionPerformed(ActionEvent e)
				{
				for(Runnable r : onTick)
					r.run();
				}
			});
		tickTimer.start();

		onTick.add(new Runnable()
			{
			@Override
			public void run()
				{
				updateStatus();
				}
			});

		worker = new Worker();
		worker.start();

		worker.add(new BP2SelfTest());

		database = new BP2GrowDB(System.getProperty("user.home"), ".blindpass2db");
		worker.add(database);
		}

	@Override
	public void windowActivated(WindowEvent e) { }

	@Override
	public void windowClosed(WindowEvent e) { }

	@Override
	public void windowClosing(WindowEvent e)
		{
		userQuit();
		}

	@Override
	public void windowDeactivated(WindowEvent e) { }

	@Override
	public void windowDeiconified(WindowEvent e) { }

	@Override
	public void windowIconified(WindowEvent e) { }

	@Override
	public void windowOpened(WindowEvent e)
		{
		SplashScreen splash = SplashScreen.getSplashScreen();
		if(splash != null)
			splash.close();
		}

	private static void handleException(Main main, Throwable ex)
		{
		try
			{
			StringBuilder sb = new StringBuilder();
			for(Throwable e = ex; e != null; e = e.getCause())
				{
				if(sb.length() > 0)
					sb.append("\n");
				String s = e.toString();
				sb.append(e.toString());
				sb.append("\n");
				for(StackTraceElement t : e.getStackTrace())
					{
					sb.append("  ");
					sb.append(t.toString());
					sb.append("\n");
					}
				}
			System.err.print(sb.toString());

			try
				{
				JTextArea txt = new JTextArea(sb.toString());
				txt.setLineWrap(true);
				try { txt.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 10)); }
				catch(Exception ignore) { }

				JScrollPane jsp = new JScrollPane(txt);
				jsp.setPreferredSize(new Dimension(480, 160));

				JOptionPane.showMessageDialog(main.layout.frame.getContentPane(),
					jsp, "Unhandled Exception", JOptionPane.ERROR_MESSAGE);
				}
			finally { main.shutdown(); }
			}
		finally { System.exit(1); }
		}

	public static void main(String[] args)
		{
		final Main main = new Main();

		SwingUtilities.invokeLater(main);

		Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler()
			{
			@Override
			public void uncaughtException(Thread thread, Throwable ex)
				{
				final Throwable e = ex;
				SwingUtilities.invokeLater(new Runnable()
					{
					@Override
					public void run()
						{
						handleException(main, e);
						}
					});
				}
			});

		}
	}

// Copyright (C)2014 by Aaron Suen <warr1024@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package com.gmail.warr1024.blindpass2.gui;

import com.gmail.warr1024.blindpass2.BP2Task;
import com.gmail.warr1024.blindpass2.BP2TaskRunner;
import java.lang.Thread;

public class Worker
	{
	private BP2TaskRunner runner = new BP2TaskRunner();
	private Exception thrown = null;
	private Thread thread = null;

	public String[] getStatus()
		{
		if(thrown != null)
			throw new RuntimeException(thrown);
		return runner.getStatus();
		}

	public void start()
		{
		if((thread != null) && thread.isAlive())
			return;

		final Worker self = this;
		thread = new Thread(new Runnable()
			{
			@Override
			public void run()
				{
				try { runner.run(); }
				catch(Exception ex) { self.thrown = ex; }
				}
			});
		thread.setDaemon(true);
		thread.start();
		}

	public void add(BP2Task job)
		{
		runner.add(job);
		}

	public void stop()
		{
		runner.stop();
		}

	public void stopWait()
		{
		stop();
		if((thread != null) && thread.isAlive())
			try { thread.join(); }
			catch(Exception ex) { throw new RuntimeException(ex); }
		if(thrown != null)
			throw new RuntimeException(thrown);
		}
	}

// Copyright (C)2014 by Aaron Suen <warr1024@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package com.gmail.warr1024.blindpass2.gui;

import com.gmail.warr1024.blindpass2.BP2Utility;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.net.URI;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;

public class Layout
	{
	public JFrame frame = null;
	public CardLayout cards = null;
	public JProgressBar barProgress = null;
	public JLabel lblProgress = null;
	public JPanel pnlCards = null;
	public JPanel pnlCopy = null;
	public JButton btnCopyOK = null;
	public JButton btnCopyQuit = null;
	public JPanel pnlMaster = null;
	public JPasswordField txtMaster1 = null;
	public JPasswordField txtMaster2 = null;
	public JButton btnMasterOK = null;
	public JButton btnMasterCancel = null;
	public JPanel pnlService = null;
	public JTextField txtService = null;
	public JButton btnServiceOK = null;
	public JButton btnServiceCancel = null;
	public JPanel pnlOpts = null;
	public JButton btnOptShow = null;
	public JButton btnOptWrite = null;
	public JButton btnOptDelete = null;
	public JButton btnOptEnd = null;
	public JPanel pnlRead = null;
	public JList<String> lstRead = null;
	public JButton btnReadEnd = null;
	public JPanel pnlWrite = null;
	public JPasswordField txtSvcPass1 = null;
	public JPasswordField txtSvcPass2 = null;
	public JSlider sldStop = null;
	public JLabel lblEstimates = null;
	public JButton btnWriteOK = null;
	public JButton btnWriteCancel = null;
	public JLabel lblJreInfo = null;

	public JButton btnCancel = null;

	private static final String CARD_COPY = "copy";
	private static final String CARD_MASTER = "master";
	private static final String CARD_SERVICE = "service";
	private static final String CARD_OPTS = "opts";
	private static final String CARD_READ = "read";
	private static final String CARD_WRITE = "write";

	public void cardCopy()
		{
		cards.show(pnlCards, CARD_COPY);
		frame.getRootPane().setDefaultButton(btnCopyOK);
		btnCancel = btnCopyQuit;
		btnCopyOK.requestFocusInWindow();
		}
	public void cardMaster()
		{
		cards.show(pnlCards, CARD_MASTER);
		frame.getRootPane().setDefaultButton(btnMasterOK);
		btnCancel = btnMasterCancel;
		txtMaster1.requestFocusInWindow();
		}
	public void cardService()
		{
		cards.show(pnlCards, CARD_SERVICE);
		frame.getRootPane().setDefaultButton(btnServiceOK);
		btnCancel = btnServiceCancel;
		txtService.requestFocusInWindow();
		}
	public void cardOpts()
		{
		cards.show(pnlCards, CARD_OPTS);
		frame.getRootPane().setDefaultButton(null);
		btnCancel = btnOptEnd;
		btnOptShow.requestFocusInWindow();
		}
	public void cardRead()
		{
		cards.show(pnlCards, CARD_READ);
		frame.getRootPane().setDefaultButton(btnReadEnd);
		btnCancel = btnReadEnd;
		btnReadEnd.requestFocusInWindow();
		}
	public void cardWrite()
		{
		cards.show(pnlCards, CARD_WRITE);
		frame.getRootPane().setDefaultButton(btnWriteOK);
		btnCancel = btnWriteCancel;
		txtSvcPass1.requestFocusInWindow();
		}

	private JPanel addCard(String name)
		{
		JPanel p = new JPanel();
		pnlCards.add(p);
		cards.addLayoutComponent(p, name);
		return p;
		}

	private void layoutGrid(Container ctr, boolean margins,
		int rows, int cols, Component... comps)
		{
		if(comps.length != (rows * cols))
			throw new RuntimeException("Expected " + (rows * cols)
				+ " total control, but got " + comps.length + ".");

		for(Component c : comps)
			if(c != null)
				ctr.add(c);

		GroupLayout gl = new GroupLayout(ctr);
		ctr.setLayout(gl);
		gl.setAutoCreateGaps(true);
		gl.setAutoCreateContainerGaps(margins);

		GroupLayout.SequentialGroup h = gl.createSequentialGroup();
		for(int x = 0; x < cols; x++)
			{
			GroupLayout.ParallelGroup hv = gl.createParallelGroup();
			h.addGroup(hv);
			for(int y = 0; y < rows; y++)
				if(comps[y * cols + x] != null)
					hv.addComponent(comps[y * cols + x]);
			}
		gl.setHorizontalGroup(h);

		GroupLayout.SequentialGroup v = gl.createSequentialGroup();
		for(int y = 0; y < rows; y++)
			{
			GroupLayout.ParallelGroup vh = gl.createParallelGroup((cols > 1)
				? GroupLayout.Alignment.BASELINE
				: GroupLayout.Alignment.LEADING);
			v.addGroup(vh);
			for(int x = 0; x < cols; x++)
				if(comps[y * cols + x] != null)
					vh.addComponent(comps[y * cols + x]);
			}
		gl.setVerticalGroup(v);
		}

	private JPanel flowPanel(int align, int gap, Component... comps)
		{
		JPanel p = new JPanel();
		p.setLayout(new FlowLayout(align, gap, gap));
		for(Component c : comps)
			p.add(c);
		return p;
		}

	private JButton hyperlink(String url, String txt)
		{
		if(txt == null)
			txt = url;
		final URI uri;
		try { uri = new URI(url); }
		catch(Exception ex) { throw new RuntimeException(ex); }
		JButton b = new JButton();
		b.setText(String.format("<html><a href=\"%s\">%s</a>", url, txt));
		b.setBorder(null);
		b.setBorderPainted(false);
		b.setOpaque(false);
		b.setContentAreaFilled(false);
		b.setToolTipText(url);
		b.setHorizontalAlignment(SwingConstants.LEFT);
		b.setCursor(new Cursor(Cursor.HAND_CURSOR));
		b.setMargin(new Insets(0, 0, 0, 0));
		b.addActionListener(new ActionListener()
			{
			@Override
			public void actionPerformed(ActionEvent e)
				{
				if(!Desktop.isDesktopSupported())
					return;
				try { Desktop.getDesktop().browse(uri); }
				catch(Exception ex) { }
				}
			});
		return b;
		}

	public Layout()
		{
		frame = new JFrame(BP2Utility.PRODUCT);
		frame.setResizable(false);

		JPanel content = new JPanel();
		content.setLayout(new BoxLayout(content, BoxLayout.Y_AXIS));
		content.setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));
		frame.setContentPane(content);

		barProgress = new JProgressBar();
		barProgress.setPreferredSize(new Dimension(32, 32));
		lblProgress = new JLabel("W");
		lblProgress.setMinimumSize(new Dimension(0, 32));
		JPanel pnlProgress = flowPanel(FlowLayout.LEADING, 5,
			barProgress, lblProgress);
		pnlProgress.setBorder(BorderFactory.createEtchedBorder());
		content.add(pnlProgress);

		content.add(Box.createRigidArea(new Dimension(0, 8)));

		pnlCards = new JPanel();
		pnlCards.setBorder(BorderFactory.createEtchedBorder());
		cards = new CardLayout();
		pnlCards.setLayout(cards);
		content.add(pnlCards);

		pnlCopy = addCard(CARD_COPY);
		
		ArrayList<Component> copyContent = new ArrayList<Component>();
		copyContent.add(flowPanel(FlowLayout.LEADING, 0,
			new JLabel(BP2Utility.COPYRIGHT + " <"),
			hyperlink("mailto:" + BP2Utility.EMAIL, BP2Utility.EMAIL),
			new JLabel(">")));
		for(String s : BP2Utility.LICENSE)
			{
			JLabel l = new JLabel("<html><pre>" + s + "</pre>");
			l.setFont(new Font(l.getFont().getName(), Font.PLAIN, 8));
			copyContent.add(flowPanel(FlowLayout.LEADING, 0, l));
			}
		btnCopyOK = new JButton("Continue");
		btnCopyQuit = new JButton("Quit");
		copyContent.add(flowPanel(FlowLayout.TRAILING, 0,
			btnCopyOK, btnCopyQuit));
		layoutGrid(pnlCopy, true, copyContent.size(), 1,
			copyContent.toArray(new Component[0]));

		pnlMaster = addCard(CARD_MASTER);

		lblJreInfo = new JLabel("W");
		
		JPanel pnlSignin = new JPanel();
		txtMaster1 = new JPasswordField();
		txtMaster1.setPreferredSize(new Dimension(240, 16));
		txtMaster2 = new JPasswordField();
		txtMaster2.setPreferredSize(new Dimension(240, 16));
		btnMasterOK = new JButton("Sign In");
		btnMasterCancel = new JButton("Back");
		JPanel pnlMasterButtons = flowPanel(FlowLayout.LEADING, 0,
			btnMasterOK, btnMasterCancel);
		layoutGrid(pnlSignin, false, 3, 2,
			new JLabel("Master Password: "), txtMaster1,
			new JLabel("Master Password (Repeat):"), txtMaster2,
			null, pnlMasterButtons);

		layoutGrid(pnlMaster, true, 4, 1, 
			hyperlink(BP2Utility.WEBSITE, null),
			lblJreInfo,
			Box.createRigidArea(new Dimension(0, 8)),
			pnlSignin);

		pnlService = addCard(CARD_SERVICE);

		txtService = new JTextField();
		txtService.setPreferredSize(new Dimension(240, 16));
		btnServiceOK = new JButton("Start Search");
		btnServiceCancel = new JButton("Sign Out");
		JPanel pnlServiceButtons = flowPanel(FlowLayout.LEADING, 0,
			btnServiceOK, btnServiceCancel);
		layoutGrid(pnlService, true, 2, 2,
			new JLabel("Service Name: "), txtService,
			null, pnlServiceButtons);

		pnlOpts = addCard(CARD_OPTS);
		btnOptShow = new JButton("Read Password(s)");
		btnOptShow.setMnemonic(KeyEvent.VK_R);
		btnOptWrite = new JButton("Write New Password");
		btnOptWrite.setMnemonic(KeyEvent.VK_W);
		btnOptDelete = new JButton("Delete Password(s)");
		btnOptDelete.setMnemonic(KeyEvent.VK_D);
		btnOptEnd = new JButton("Stop Search");
		layoutGrid(pnlOpts, true, 5, 1, btnOptShow, btnOptWrite,
			btnOptDelete, Box.createRigidArea(new Dimension(0, 4)),
			btnOptEnd);

		pnlRead = addCard(CARD_READ);
		lstRead = new JList<String>(new DefaultListModel<String>());
		lstRead.setLayoutOrientation(JList.VERTICAL);
		lstRead.setVisibleRowCount(-1);
		JScrollPane scpRead = new JScrollPane(lstRead);
		scpRead.setPreferredSize(new Dimension(320, 120));
		btnReadEnd = new JButton("Done");
		layoutGrid(pnlRead, true, 2, 1, scpRead, btnReadEnd);
		
		pnlWrite = addCard(CARD_WRITE);
		JPanel pnlWritePasses = new JPanel();
		txtSvcPass1 = new JPasswordField();
		txtSvcPass1.setPreferredSize(new Dimension(240, 16));
		txtSvcPass2 = new JPasswordField();
		txtSvcPass2.setPreferredSize(new Dimension(240, 16));
		layoutGrid(pnlWritePasses, false, 2, 2,
			new JLabel("Service Password: "), txtSvcPass1,
			new JLabel("Service Password (Repeat): "), txtSvcPass2);
		sldStop = new JSlider(JSlider.HORIZONTAL, 1, 50, 1);
		sldStop.setMajorTickSpacing(7);
		sldStop.setMinorTickSpacing(1);
		sldStop.setPaintTicks(true);
		sldStop.setPaintLabels(true);
		lblEstimates = new JLabel("W");
		btnWriteOK = new JButton("Save");
		btnWriteCancel = new JButton("Cancel");
		JPanel pnlWriteButtons = flowPanel(FlowLayout.TRAILING, 0,
			btnWriteOK, btnWriteCancel);
		layoutGrid(pnlWrite, true, 4, 1,
			pnlWritePasses, sldStop, lblEstimates, pnlWriteButtons);

		frame.pack();
		lblProgress.setText("");
		lblJreInfo.setText("");
		lblEstimates.setText("Calculating...");
		frame.setLocationRelativeTo(null);

		final String CANCEL_ACTION_KEY = "CANCEL_ACTION_KEY";
		frame.getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
			.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, false), CANCEL_ACTION_KEY);
		frame.getRootPane().getActionMap().put(CANCEL_ACTION_KEY, new AbstractAction()
			{
			public static final long serialVersionUID = 0;

			@Override
			public void actionPerformed(ActionEvent e)
				{
				if(btnCancel != null)
					btnCancel.doClick();
				}
			});
		}
	}

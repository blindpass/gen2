// Copyright (C)2014 by Aaron Suen <warr1024@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package com.gmail.warr1024.blindpass2.gui;

//import com.gmail.warr1024.blindpass2.BP2Database;
//import com.gmail.warr1024.blindpass2.BP2GrowDB;
//import com.gmail.warr1024.blindpass2.BP2Master;
//import com.gmail.warr1024.blindpass2.BP2Random;
import com.gmail.warr1024.blindpass2.BP2Result;
import com.gmail.warr1024.blindpass2.BP2Search;
import com.gmail.warr1024.blindpass2.BP2Utility;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//import java.awt.event.WindowEvent;
//import java.awt.event.WindowListener;
//import java.awt.SplashScreen;
//import java.lang.StringBuilder;
//import java.lang.Thread;
import java.nio.CharBuffer;
//import java.util.ArrayList;
//import javax.swing.DefaultListModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
//import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
//import javax.swing.Timer;
//import javax.swing.WindowConstants;

public class UIWrite implements Runnable
	{
	private class PendingWrite
		{
		public BP2Search search;
		public Runnable action;
		public char[] pass;

		public void destroy()
			{
			synchronized(this)
				{
				search.removeListener(this.action);
				BP2Utility.destroyChars(pass);
				pass = null;
				}
			}
		}

	private Object pendWriteLock = new Object();
	private PendingWrite pendWrite = null;
	private Main main = null;

	public UIWrite(Main newMain)
		{
		main = newMain;
		}

	public void updateEstimates()
		{
		Layout l = main.getLayout();
		if((l == null) || !l.lblEstimates.isShowing())
			return;

		BP2Search s = main.getSearch();
		if(s == null)
			return;

		int stop = l.sldStop.getValue();
		double rs = s.getEstimateSeconds(stop, false);
		double ws = s.getEstimateSeconds(stop, true);
		l.lblEstimates.setText(String.format("Stop %d, Write %s, Read %s",
			stop, BP2Utility.secondsDisplay(ws), BP2Utility.secondsDisplay(rs)));
		}

	private void enableWriteUI(boolean enable)
		{
		final Layout layout = main.getLayout();
		layout.txtSvcPass1.setEnabled(enable);
		layout.txtSvcPass2.setEnabled(enable);
		layout.sldStop.setEnabled(enable);
		layout.btnWriteOK.setEnabled(enable);
		}

	private void endWrite()
		{
		final Layout layout = main.getLayout();
		layout.txtSvcPass1.setText("");
		layout.txtSvcPass2.setText("");
		layout.sldStop.setValue(1);

		synchronized(pendWriteLock)
			{
			if(pendWrite != null)
				pendWrite.destroy();
			pendWrite = null;
			}

		enableWriteUI(true);
		}

	private void finishWrite(PendingWrite pw, int setstop)
		{
		synchronized(pendWriteLock)
			{
			if(pw.pass == null)
				return;
			int stop = 0;
			BP2Result found = null;
			for(BP2Result r : pw.search.getResults())
				if(!r.getExists())
					{
					stop++;
					if(stop >= setstop)
						{
						found = r;
						break;
						}
					}
			if(found == null)
				return;
			for(BP2Result r : pw.search.getResults())
				if(r.getExists())
					r.removeContent();
			found.putContent(pw.pass);
			pw.destroy();

			SwingUtilities.invokeLater(new Runnable()
				{
				@Override
				public void run()
					{
					endWrite();
					main.setSearch(null);
					main.getLayout().cardService();
					main.alertInfo("Save completed.");
					}
				});
			}
		}

	@Override
	public void run()
		{
		final Layout layout = main.getLayout();

		main.getOnTick().add(new Runnable()
			{
			@Override
			public void run()
				{
				updateEstimates();
				}
			});

		layout.btnWriteCancel.addActionListener(new ActionListener()
			{
			@Override
			public void actionPerformed(ActionEvent e)
				{
				endWrite();
				layout.cardOpts();
				layout.lblEstimates.setText("Calculating...");
				}
			});

		layout.btnWriteOK.addActionListener(new ActionListener()
			{
			@Override
			public void actionPerformed(ActionEvent e)
				{
				char[] spwd = null, rpwd = null;
				try
					{
					spwd = layout.txtSvcPass1.getPassword();
					rpwd = layout.txtSvcPass2.getPassword();
					if(!CharBuffer.wrap(spwd).equals(CharBuffer.wrap(rpwd)))
						{
						if(layout.txtSvcPass1.isFocusOwner()
							&& (rpwd.length < 1))
							{
							layout.txtSvcPass2.requestFocusInWindow();
							return;
							}
						main.alertError("Service passwords do not match.");
						return;
						}
					if(spwd.length < 1)
						{
						main.alertError("Service password cannot be blank.");
						return;
						}

					if(!main.confirm("<html>Any existing password(s) for this service found "
						+ "so far<br>will be deleted when saving the new password.  "
						+ "Continue?", "Overwrite Existing Password(s)"))
						return;

					layout.txtSvcPass1.setText("");
					layout.txtSvcPass2.setText("");

					enableWriteUI(false);

					final int setstop = layout.sldStop.getValue();
					final PendingWrite pw = new PendingWrite();
					pw.search = main.getSearch();
					pw.action = new Runnable()
						{
						@Override
						public void run()
							{
							finishWrite(pw, setstop);
							}
						};
					pw.pass = new char[spwd.length];
					System.arraycopy(spwd, 0, pw.pass, 0, spwd.length);
					pendWrite = pw;
					pw.search.addListener(pw.action, true);

					SwingUtilities.invokeLater(new Runnable()
						{
						@Override
						public void run()
							{
							int waiting = setstop;
							for(BP2Result r : pw.search.getResults())
								if(!r.getExists())
									waiting--;
							if(waiting > 0)
								main.alertInfo("Save will complete when search reaches "
									+ "your chosen stop.");
							}
						});
					}
				finally { BP2Utility.destroyChars(spwd, rpwd); }
				}
			});

		layout.sldStop.addChangeListener(new ChangeListener()
			{
			@Override
			public void stateChanged(ChangeEvent e)
				{
				updateEstimates();
				}
			});
		}
	}

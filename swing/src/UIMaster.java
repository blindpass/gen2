// Copyright (C)2014 by Aaron Suen <warr1024@gmail.com>
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy of
// this software and associated documentation files (the "Software"), to deal in
// the Software without restriction, including without limitation the rights to
// use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
// the Software, and to permit persons to whom the Software is furnished to do so,
// subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
// IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
// CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

package com.gmail.warr1024.blindpass2.gui;

import com.gmail.warr1024.blindpass2.BP2Master;
import com.gmail.warr1024.blindpass2.BP2Utility;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.CharBuffer;

class UIMaster implements Runnable
	{
	public Main main = null;

	public UIMaster(Main newMain)
		{
		main = newMain;
		}

	@Override
	public void run()
		{
		final Layout layout = main.getLayout();

		layout.btnMasterCancel.addActionListener(new ActionListener()
			{
			@Override
			public void actionPerformed(ActionEvent e)
				{
				layout.cardCopy();
				}
			});

		layout.btnMasterOK.addActionListener(new ActionListener()
			{
			@Override
			public void actionPerformed(ActionEvent e)
				{
				char[] mpwd = null, rpwd = null;
				try
					{
					mpwd = layout.txtMaster1.getPassword();
					rpwd = layout.txtMaster2.getPassword();
					if(!CharBuffer.wrap(mpwd).equals(CharBuffer.wrap(rpwd)))
						{
						if(layout.txtMaster1.isFocusOwner()
							&& (rpwd.length < 1))
							{
							layout.txtMaster2.requestFocusInWindow();
							return;
							}
						main.alertError("Master passwords do not match.");
						return;
						}
					if(mpwd.length < 1)
						{
						main.alertError("Master password cannot be blank.");
						return;
						}
					layout.txtMaster1.setText("");
					layout.txtMaster2.setText("");
					main.setMaster(new BP2Master(mpwd));
					layout.cardService();
					}
				finally { BP2Utility.destroyChars(mpwd, rpwd); }
				}
			});
		}
	}
